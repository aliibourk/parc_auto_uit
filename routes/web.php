<?php

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VttController;
use App\Http\Controllers\testcontroller;
use App\Http\Controllers\UserController;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\GarageController;
use App\Http\Controllers\MissionController;
use App\Http\Controllers\VehiculeController;
use App\Http\Controllers\VignetteController;
use App\Http\Controllers\CarburantController;
use App\Http\Controllers\ChaufeureController;
use App\Http\Controllers\EntretienController;
use App\Http\Controllers\AssauranceController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ReparationController;
use App\Http\Controllers\CarnetCarburantController;
use App\Http\Controllers\VisiteTechniqueController;
use App\Http\Controllers\Entreprise_assaurance_Controller;

//
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


  
// Route::group(['middleware' => ['auth']], function() {
//     Route::resource('roles', RoleController::class);
//     Route::resource('users', UserController::class);
//     Route::resource('products', ProductController::class);
// });





Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('0');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// ---------------------------Admins----------------------

Route::group(['middleware' => ['role:Super Admin|Admin']], function () {

    Route::get('permission/{role}/{permission}/', [PermissionController::class, 'togglePrmission'])->name('toogle');
    Route::resource('Users', UserController::class)->except(['show'])->middleware('auth');
    Route::get('add_users', [UserController::class, 'create'])->name('add_users');
    Route::post('store_users', [UserController::class, 'store'])->name('store_users');

    Route::PUT('update_user/{id}', [UserController::class, 'update'])->name('update_user');

    Route::get('permission', [PermissionController::class, 'index'])->name('permission');
    // Route::get('tst', [testcontroller::class, 'index'])->name('tst');

});

// ---------------------------Vehicules----------------------

Route::resource('vehicules', VehiculeController::class)->except(['show'])->middleware('auth');
Route::get('vehicules/archive', [VehiculeController::class, 'archive'])->middleware('auth')->name('archiveVehicule');
Route::patch('vehicules/{id}/restore', [VehiculeController::class, 'restore'])->middleware('auth')->name('restoreVehicule');
Route::delete('vehicules/{id}/forcedelete', [VehiculeController::class, 'forcedelete'])->middleware('auth')->name('forcedelete');

// ---------------------------Chaufeures---------------------------------------------------------------

Route::resource('chaufeures', ChaufeureController::class)->except(['show'])->middleware('auth');
Route::get('chaufeures/archive', [ChaufeureController::class, 'archive'])->middleware('auth')->name('archiveChaufeure');
Route::patch('chaufeures/{id}/restore', [ChaufeureController::class, 'restore'])->middleware('auth')->name('restoreChaufeure');

// ----------------------------Carnet Carburant--------------------------------------------------------
Route::resource('CarnetCarburant', CarnetCarburantController::class)->except(['show','edit','destroy','update'])->middleware('auth');

Route::get('CarnetCarburant/archive', [CarnetCarburantController::class, 'archive'])->middleware('auth')->name('archivecarnet');

// ----------------------------Mission-----------------------------------------------------------------
Route::resource('mission', MissionController::class)->except(['show'])->middleware('auth');

// ----------------------------Garages-----------------------------------------------------------------
Route::resource('Garages', GarageController::class)->except(['show'])->middleware('auth');

// ----------------------------Reparation--------------------------------------------------------------
Route::resource('Reparation', ReparationController::class)->except(['show'])->middleware('auth');

// ----------------------------Assaurance--------------------------------------------------------------
Route::resource('Assaurance', AssauranceController::class)->except(['show'])->middleware('auth');

// ----------------------------Entreprise Assaurance---------------------------------------------------
Route::resource('Entreprise_assaurance', Entreprise_assaurance_Controller::class)->except(['show'])->middleware('auth');

// ----------------------------Vignette----------------------------------------------------------------
Route::resource('Vignette', VignetteController::class)->except(['show'])->middleware('auth');

// ----------------------------Carburant---------------------------------------------------------------
Route::resource('Carburant', CarburantController::class)->except(['show'])->middleware('auth');

// ----------------------------Visite Technique--------------------------------------------------------
Route::resource('VisiteTechnique', VisiteTechniqueController::class)->except(['show'])->middleware('auth');

// ----------------------------Visite Technique--------------------------------------------------------
Route::resource('VTT', VttController::class)->except(['show'])->middleware('auth');

// ----------------------------Entretien---------------------------------------------------------------
Route::resource('Entretien', EntretienController::class)->except(['show'])->middleware('auth');
// ->only('index','store','create','destroy');

// ----------------------------export---------------------------------------------------------------
Route::get('/Vehicules_to_excel', [App\Http\Controllers\VehiculeController::class, 'exportexcel'])->middleware('auth');
// Route::get('/Vehicules_to_csv', [App\Http\Controllers\VehiculeController::class, 'exportCSV'])->middleware('auth');

// ----------------------------Auth---------------------------------------------------------------

Auth::routes(['register' => false]);


// Route::get('/login', [LoginController::class, 'authenticated'])->name('login');
