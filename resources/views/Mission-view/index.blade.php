@extends('layouts.app')

@section('title')
    index Missions
@endsection

@section('content')

    <center>
        <h1 class="mt-4">LISTE DES MISSIONS</h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/mission">Missions</a> </li>
        </ol>
    </div>

    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                <div class="table-responsive">
                    {{-- class="table-info" --}}
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <td style="border:none;white-space: nowrap" class="text-uppercase">#</td>
                            <td style="border:none;white-space: nowrap" class="text-uppercase">Vehicule</td>
                            <td style="border:none;white-space: nowrap" class="text-uppercase">Chauffeure</td>
                            <td style="border:none;white-space: nowrap" class="text-uppercase">Date debut</td>
                            <td style="border:none;white-space: nowrap" class="text-uppercase">Date fin</td>
                            <td style="border:none;white-space: nowrap" class="text-uppercase">Montant</td>
                            @can('edit mission')
                                <td class="text-right text-uppercase" style="border:none;white-space: nowrap">Action</td>
                            @endcan

                        </thead>
                        <tbody id="myTable">
                            @forelse($Missions as $Mission)
                                <tr>
                                    <td>
                                        <a>{{ $Mission->id }}</a>
                                    </td>
                                    <td>
                                        @if ($Mission->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($Mission->vehicule_id == $vehicule->id)
                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif
                                                    {{ $vehicule->type }} : {{ $vehicule->marque }}
                                                    {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Mission->chauffeure_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_chauffeure as $chauffeure)
                                                @if ($Mission->chauffeure_id == $chauffeure->id)
                                                    {{ $chauffeure->prenom }} {{ $chauffeure->nom }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Mission->date_debut == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Mission->date_debut }}
                                        @endif
                                    </td>
                                    <td> 
                                        @if ($Mission->date_fin == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Mission->date_fin }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Mission->montant == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Mission->montant }} DH
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <center>
                                            @can('edit mission')
                                                <a href="{{ route('mission.edit', $Mission->id), '/edit' }}" class="Edit"
                                                    title="Modifier" data-toggle="tooltip"><i
                                                        class="fas fa-edit fa-lg"></i></i></a>
                                            @endcan
                                            {{-- <a href="#" class="delete" title="Supprimer" data-toggle="modal"
                                                data-target="#exampleModal-{{ $Mission->id }}"
                                                data-whatever="{{ $Mission->id }}"><i
                                                    class="fas fa-trash-alt fa-lg"></i></a> --}}

                                            {{-- Model --}}
                                            {{-- <div class="modal fade" id="exampleModal-{{ $Mission->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form method="POST" action="{{ route('mission.destroy', $Mission->id) }}"
                                                    role="form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    confirmation</h5>
                                                                <input type="hidden" name="dele"
                                                                    value="{{ $Mission->id }}">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3> voulez vous supprimer la Mission Referance :
                                                                    {{ $Mission->id }}</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-danger btn-send"
                                                                    value="Supprimer">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> --}}
                                        </center>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">
                                        <div class="card-header">
                                            <strong style="color: red">
                                                <center>Aucune Mission</center>
                                            </strong>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
