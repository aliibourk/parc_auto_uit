@extends('layouts.app')

@section('title')
    Edit Mission
@endsection

@section('content')
    <center>
        <h1 class="mt-4"><span class="text-uppercase">MODIFIER MISSION</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/mission">Missions &nbsp;</a><a href="{{ route('mission.edit', $Mission->id), '/edit' }}">/ Modifier</a>
            </li>
        </ol>
    </div>


    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Modifier Mission : Referance {{ $Mission->id }}</h3>
                </div>
                <div class="card-body">

                    <form id="add-form" method="POST" action="{{ route('mission.update', $Mission->id) }}" role="form">
                        @csrf
                        @method('PUT')
                        <div class="messages"></div>
                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule : <span style="color: red"> *</label>
                                        <select id="vehicule_id" disabled class="form-control">
                                            <option selected>{{ $MyVehicule->type }} : {{ $MyVehicule->marque }} {{ $MyVehicule->immatriculation }}</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chauffeure_id">Chauffeure : <span style="color: red"> *</label>
                                        <select id="chauffeure_id" disabled class="form-control">
                                            <option selected> {{ $MyChauffeure->nom }} {{ $MyChauffeure->prenom }}</option>
                                        </select>
                                        <span style="color: red">@error('chauffeure_id'){{ $message }}@enderror</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_debut">Date Debut : <span style="color: red"> *</span></label>
                                            <input id="date_debut" type="date" value="{{ $Mission->date_debut }}"
                                                name="date_debut" class="form-control @if ($errors->get('date_debut')) border border-danger @endif" required="required" >
                                            <span style="color: red">@error('date_debut'){{ $message }}@enderror</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="date_fin">Date Fin : <span style="color: red"> *</label>
                                                <input id="date_fin" name="date_fin" type="date" value="{{ $Mission->date_fin }}"
                                                    class="form-control @if ($errors->get('date_fin')) border border-danger @endif" required="required">
                                                <span style="color: red">@error('date_fin'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="montant">Montant : </label>
                                                    <div class="input-group mb-3">
                                                        <input id="montant" type="number" name="montant"
                                                            value="{{ $Mission->montant }}" class="form-control @if ($errors->get('montant')) border border-danger @endif" placeholder="montant ">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">DH</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="color: red">@error('montant'){{ $message }}@enderror</span>
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="submit" class="btn btn-primary btn-send" value="Modifier">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-muted">
                                                        <strong><span style="color: red">* est obligatoire.</span></strong>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endsection
