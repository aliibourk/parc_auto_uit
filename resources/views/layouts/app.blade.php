<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- template style --}}
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    {{-- Table style --}}
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">


    {{-- <link rel="preconnect" href="https://fonts.gstatic.com"> --}}
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

    {{-- <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap" rel="stylesheet"> --}}
    {{-- <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&family=Montserrat:wght@500&display=swap" rel="stylesheet"> --}}
    {{-- <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&family=Saira:wght@500&display=swap" rel="stylesheet"> --}}

    <style>
        table.table td a.settings {
            color: #2196F3;
        }

        table.table td a.delete {
            color: #F44336;
        }

        table.table td a.Restore {
            color: #36a12d;
        }

        body {
            /* font-family: 'Jost', sans-serif; */
            font-family: 'Montserrat', sans-serif;
            /* font-family: 'Saira', sans-serif; */

            /* color:black; */
            /* text-decoration-color: black; */
            /* background: #961d1d; */
            font-size: 8px;
        }

    </style>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        {{-- <a class="navbar-brand" href="">Parc Auto</a> --}}

        <a class="navbar-brand" href="#">
            {{-- <img src="/img/logo-uit.png" width="30" height="30" class="d-inline-block align-top" alt=""> --}}
            Parc Auto
        </a>

        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i
                class="fas fa-bars"></i></button>
        <!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <a class="navbar-brand" href="">Parc Auto Présidence UIT Kénitra</a>
            {{-- <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div> --}}
        </form>
        <!-- Navbar-->
        {{-- <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="">Logout</a>
                    </div>
                </li>
            </ul> --}}
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @endif

                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        <span style="color: rgb(255, 255, 255)"> <i class="fas fa-user fa-fw"></i>
                            {{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @role('Super Admin|Admin')
                            <a class="dropdown-item" href="{{ url('/Users')}}">Users</a>
                            <a class="dropdown-item" href="{{ route('permission')}}">Permission</a>
                        @endrole

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>                    

                    </div>
                </li>
            @endguest
        </ul>
    </nav>

    <div id="layoutSidenav">
        @auth
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">interfaces</div>
                            <!-- mission -->
                            @can('select mission')
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMission"
                                    aria-expanded="false" aria-controls="collapseMission">
                                    <div class="sb-nav-link-icon"><i class="fas fa-location-arrow"></i></div>
                                    Missions
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapseMission" aria-labelledby="headingOne"
                                    data-parent="#sidenavAccordion">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ url('/mission') }}">Liste Mission</a>
                                        @can('create mission')
                                            <a class="nav-link" href="{{ route('mission.create') }}">Ajouter Mission</a>    
                                        @endcan
                                    </nav>
                                </div>
                            @endcan
                            
                            @can('select carburant')
                             <!-- Carburant -->
                             <a class="nav-link" href="{{ url('/Carburant') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-gas-pump"></i></div>
                                Carburants
                            </a>
                            @endcan 
                           
                            @can('select vehicule')   
                            <!-- Vehicules -->
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Vehicules"
                                aria-expanded="false" aria-controls="Vehicules">
                                <div class="sb-nav-link-icon"><i class="fas fa-car"></i></div>
                                Vehicules
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="Vehicules" aria-labelledby="headingOne"
                                data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{ url('/vehicules') }}">Liste Vehicule</a>
                                    @can('create vehicule')
                                        <a class="nav-link" href="{{ route('vehicules.create') }}">Ajouter Vehicule</a>
                                    @endcan
                                </nav>
                            </div>
                            @endcan

                            @can('select chauffeure')
                            <!-- Chauffaures -->
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Chauffaures"
                                aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                                Chauffaures
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="Chauffaures" aria-labelledby="headingOne"
                                data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{ url('/chaufeures') }}">Liste chauffaure</a>
                                    @can('create chauffeure')
                                        <a class="nav-link" href="{{ route('chaufeures.create') }}">Ajouter chauffaure</a>
                                    @endcan
                                </nav>
                            </div>
                            @endcan

                            @can('select assaurance')
                            <!-- Assaurance -->
                            <a class="nav-link" href="{{ url('/Assaurance') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-car"></i></div>
                                Assaurances
                            </a>
                            @endcan

                            @can('select vignette')
                            <!-- Vignette -->
                            <a class="nav-link" href="{{ url('/Vignette') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Vignettes
                            </a>
                            @endcan

                            @canany(['select reparation', 'select entretien', 'select visite'])

                            <!-- Interventions -->
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Interventions"
                                aria-expanded="false" aria-controls="Interventions">
                                <div class="sb-nav-link-icon"><i class="fas fa-car-crash"></i></div>
                                Interventions
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="Interventions" aria-labelledby="headingOne"
                                data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    @can('select reparation')
                                        <a class="nav-link" href="{{ url('/Reparation') }}">Reparations</a>
                                    @endcan
                                    @can('select entretien')
                                        <a class="nav-link" href="{{ url('/Entretien') }}">Entretiens</a>                  
                                    @endcan
                                    @can('select visite')
                                        <a class="nav-link" href="{{ url('/VisiteTechnique') }}">Visites Technique</a>
                                    @endcan

                                </nav>
                            </div>
                            @endcan


                            @canany(['select carnet', 'select garage', 'select entreAss'])

                            <!-- Parametre -->
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Parametre"
                                aria-expanded="false" aria-controls="Parametre">
                                <div class="sb-nav-link-icon"><i class="fas fa-cogs"></i></div>
                                Parametre d'application
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="Parametre" aria-labelledby="headingOne"
                                data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                @can('select vtt')    
                                    <a class="nav-link" href="{{ url('/VTT') }}">VTT</a>
                                @endcan
                                @can('select carnet')
                                    <a class="nav-link" href="{{ url('/CarnetCarburant') }}">Carnet carburant</a>
                                @endcan
                                @can('select garage')
                                    <a class="nav-link" href="{{ url('/Garages') }}">Garages</a>
                                @endcan
                                @can('select entreAss')
                                    <a class="nav-link" href="{{ url('/Entreprise_assaurance') }}">Entreprise d'ssaurance</a>
                                @endcan
                                    </nav>
                            </div>
                            @endcan


                        </div>
                    </div>

                    @can('exporte')
                        <div class="sb-sidenav-footer">
                            <div class="small">Exporter tout les données:</div>
                            <form method="HEAD" action="{{ url('/Vehicules_to_excel') }}" role="form">
                                @csrf
                                <input type="submit" class="btn btn-outline-warning" value='Exporter'>
                            </form>
                        </div>
                    @endcan
                    

                    {{-- <div class="sb-sidenav-footer">

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span style="color: rgb(115, 255, 0)">Exporter les données</span>
                            </a>
    
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/Vehicules_to_excel') }}"
                                   onclick="event.preventDefault();
                                         document.getElementById('Exporter-form').submit();">
                                            <i class="fas fa-file-excel"></i> Exporter
                                </a>
        
                                <form id="Exporter-form" action="{{ url('/Vehicules_to_excel') }}" method="GET" class="d-none">
                                    @csrf
                                </form>
                            </div>

                        </li>      
                    </div> --}}



                    {{-- <form method="POST" action="{{ url('/Vehicules_to_excel') }}" role="form">
                            @csrf  
                            <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModal">
                                <i class="fas fa-file-excel"></i> Exporter
                            </button>
                        </form> --}}



                </nav>
            </div>
        @endauth
        <div id="layoutSidenav_content">
            <main style="background-color: rgb(244, 243, 239)">

                {{-- @if (session()->has('status'))
                    <div class="container">
                        <div class="table-responsive">
                            <div class="table-wrapper">
                                <div class="alert alert-success" role="alert">
                                    <center>
                                        <strong>Saved!</strong> {{ session()->get('status') }}
                                    </center> 
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif --}}

                @include('Flash.MyFlash')

                @yield('content')
                <br>
                <br>
                <br>

            </main>

            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Parc Auto Présidence UIT 2021</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    {{-- <script src="{{ asset('js/all.min.js') }}" defer></script> --}}
    <script src="{{ asset('js/jquery-3.5.1.slim.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>

    <script src="{{ asset('js/scripts.js') }}" defer></script>
    <script src="{{ asset('js/jquery-dataTables.min.js') }}" defer></script>
    <script src="{{ asset('js/dataTables-bootstrap4.min.js') }}" defer></script>
    <script src="{{ asset('assets/demo/datatables-demo.js') }}" defer></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script> --}}

    <!-- <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script> -->

    {{-- <script src="assets/demo/datatables-demo.js"></script> --}}

</body>

</html>
