@extends('layouts.app')

@section('title')
Edit Entretien
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase"> MODIFIER Entretien</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Entretien">Entretiens &nbsp;</a><a
                href="{{ route('Entretien.edit', $Entretien->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header"><h3>Modifier Entretien : Referance {{$Entretien->id}}</h3>  </div>
            <div class="card-body">   

 

    <form id="add-form" method="POST" action="{{ route('Entretien.update' , $Entretien->id ) }}" role="form">
      @csrf
      @method('PUT')
      <div class="messages"></div>
      <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                    <select id="vehicule_id" disabled class="form-control" required="required">
                        <option selected>{{ $MyVehicule->type}} : {{ $MyVehicule->marque}} {{ $MyVehicule->immatriculation }}</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="chauffeure_id">Chauffeure :<span style="color: red"> *</span></label>
                    <select id="chauffeure_id" disabled class="form-control" required="required" >
                        <option selected>{{ $MyChauffeure->nom }} {{ $MyChauffeure->prenom }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="date">Date :<span style="color: red"> *</span></label>
                    <input id="date" type="date" value="{{ $Entretien->date }}" name="date" class="form-control @if($errors->get('date')) border border-danger @endif" required="required" >
                    <span style="color: red">@error('date'){{$message}}@enderror</span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="montant_Carburant">Montant Vignette</label>
                    <div class="input-group mb-3">
                        <input id="montant_Carburant" type="number" value="{{ $Entretien->montant_Carburant }}" name="montant_Carburant" class="form-control @if($errors->get('montant_Carburant')) border border-danger @endif" placeholder="montant vignette ">
                        <div class="input-group-append">
                          <span class="input-group-text">DH</span>
                        </div>
                      </div>
                      <span style="color: red">@error('montant_Carburant'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="object">Object</label>
                  <div class="input-group mb-3">
                      <input id="object" type="text" value="{{ $Entretien->object }}" name="object" class="form-control @if($errors->get('object')) border border-danger @endif" placeholder="object ">
                    </div>
                    <span style="color: red">@error('object'){{$message}}@enderror</span>
              </div>
          </div>

          <div class="col-md-6">
              <div class="form-group">
                  <label for="Garage_id">Garage :<span style="color: red"> *</span></label>
                  <select id="Garage_id" disabled class="form-control" required="required" >
                        <option selected>{{ $MyGarage->NomGarage }}</option>
                  </select>
              </div>
          </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kelometrage">Kelometrage</label>
                    <div class="input-group mb-3">
                        <input id="kelometrage" type="NUMBER" value="{{ $Entretien->kelometrage }}" name="kelometrage" class="form-control @if($errors->get('kelometrage')) border border-danger @endif" placeholder="kelometrage">
                        <div class="input-group-append">
                            <span class="input-group-text">kilomètre</span>
                          </div>
                          <span style="color: red">@error('kelometrage'){{$message}}@enderror</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="N_Souche">N Souche</label>
                    <div class="input-group mb-3">
                        <input id="N_Souche" type="text" name="N_Souche" value="{{ $Entretien->N_Souche }}" class="form-control @if($errors->get('N_Souche')) border border-danger @endif" placeholder="Numero de Souche">
                      </div>
                      <span style="color: red">@error('N_Souche'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="observation">Observation</label>
                    <textarea id="observation" name="observation" class="form-control @if($errors->get('observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ $Entretien->observation }}</textarea>
                    <span style="color: red">@error('observation'){{$message}}@enderror</span>
                </div>
            </div>
        </div>


        
     
          <div class="row">     
              <div class="col-md-12">
                  <input type="submit" class="btn btn-primary btn-send" value="Modifier">
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <p class="text-muted">
                      <strong><span style="color: red">* est obligatoire.</span></strong>
              </div>
          </div>
      </div>

  </form>
</div>

        </div>
      </div>
</div>
@endsection
