@extends('layouts.app')

@section('title')
index Entretiens 
@endsection
   
@section('content')

<center>
    <h1 class="mt-4"><span class="text-uppercase">LISTE DES Entretiens</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Entretien">Entretiens</a> </li>
    </ol>
</div>

@can('create entretien')
    <center>
        <a href="{{ route('Entretien.create') }}" style="font-size: 20px" class="btn btn-primary"><i class="fas fa-plus-square" ></i> <span>Ajouter une Entretien</span></a>
    </center>
@endcan
<BR>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead class="text-primary">
    <tr>
        <td style="border:none;white-space: nowrap" class="text-uppercase">#</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Vehicule</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Chauffeure</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Date</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Carburant</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">N Souche</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Object</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Kelometrage</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Benificiere</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Observation</td>
        @can('edit entretien')
            <td style="border:none;white-space: nowrap" class="text-uppercase">Action</td>
        @endcan
    </tr> 
</thead>
<tbody id="myTable">    
    @forelse($Entretiens as $Entretien)
    <tr>
        <td style="white-space: nowrap">
            <a>{{$Entretien->id}}</a>
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->vehicule_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_vehicule as $vehicule)
                        @if ( $Entretien->vehicule_id == $vehicule->id)

                            @if ($vehicule->type == "Voiture")
                                <i class="fas fa-car"></i>
                            @else
                                @if ($vehicule->type == "Moto" || $vehicule->type == "Velo")
                                    <i class="fas fa-bicycle"></i>    
                                @else
                                    @if ($vehicule->type == "Bus" || $vehicule->type == "MiniBus")
                                    <i class="fas fa-bus-alt"></i>                   
                                    @endif                
                                @endif
                            @endif

                            {{ $vehicule->marque}} : {{ $vehicule->immatriculation }}
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->chauffeure_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_chauffeure as $chauffeure)
                        @if ( $Entretien->chauffeure_id == $chauffeure->id)
                            {{ $chauffeure->prenom }} {{ $chauffeure->nom }}
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->date == null)
                    <span style="color: red">---</span>
                @else
                    {{$Entretien->date}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->montant_Carburant == null)
                    <span style="color: red">---</span>
                @else
                    {{$Entretien->montant_Carburant}} DH
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->N_Souche == null)
                    <span style="color: red">---</span>
                @else
                    {{$Entretien->N_Souche}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->object == null)
                    <span style="color: red">---</span>
                @else
                    {{$Entretien->object}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->kelometrage == null)
                    <span style="color: red">---</span>
                @else
                    {{$Entretien->kelometrage}} kilo
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Entretien->Garage_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_garage as $garage)
                        @if ( $garage->id == $Entretien->Garage_id)
                            {{ $garage->NomGarage }} 
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
            @if ($Entretien->observation == null)
                <span style="color: red">---</span>
            @else
                {{ mb_strimwidth("$Entretien->observation",0,10,"...")  }}
            @endif
        </td>

        @can('edit entretien')
        <td style="white-space: nowrap">
            <center>
                <a href="{{ route('Entretien.edit' , $Entretien->id ),'/edit' }}" class="Edit" title="edit" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                {{-- <a href="" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $Entretien->id }}" data-whatever="{{ $Entretien->id }}"><i class="fas fa-trash-alt fa-lg"></i></a> --}}
            
                    {{-- Model --}}
                    {{-- <div class="modal fade" id="exampleModal-{{ $Entretien->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <form method="POST" action="{{ route('Entretien.destroy' , $Entretien->id ) }}" role="form">
                            @csrf
                            @method('DELETE')
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                                <input type="hidden" name="dele" value="{{ $Entretien->id }}">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <h3> voulez vous supprimer la Entretien <br> 
                                        Referance : {{ $Entretien->id }}</h3>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                                </div>
                            </div>
                            </div>
                        </form>
                    </div> --}}

            </center>
        </td>   
        @endcan

    </tr>

    @empty
    <tr>
        <td colspan="11">
            <div class="card-header"><strong style="color: red"> <center>Aucune Entretien</center> </strong> </div>
        </td>
    </tr>
        
    @endforelse  
    
</tbody>
</table>

</div>
</div>
</div>        
</div>

@endsection
