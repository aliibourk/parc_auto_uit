@extends('layouts.app')

@section('title')
Edit User
@endsection
    
@section('content')

<center>
    <h1 class="mt-4">
        <span class="text-uppercase">Modifier une Staff</span>
    </h1>
    <br>
</center>

<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">
            <a href="/Users">Users &nbsp;</a> <a href="{{ route('Users.edit' , $User->id ),'/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modifier {{ $User->name }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('update_user', $User->id) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" disabled type="text" class="form-control" name="name" value="{{ $User->name }}" required autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" disabled type="email" class="form-control" name="email" value="{{ $User->email }}" required autocomplete="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Disponible" class="col-md-4 col-form-label text-md-right">Disponible</label>

                            <div class="form-row align-items-center">

                                <div class="col-auto">
                                    <div class="form-check mb-2">
                                        @if ($User->Status)
                                            <input class="form-check-input" value="1" type="radio"
                                                id="Status" name="Status" checked>
                                        @else
                                            <input class="form-check-input" value="1" type="radio"
                                                id="Status" name="Status">
                                        @endif
                                        <label class="form-check-label" for="autoSizingCheck">Oui</label>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-check mb-2">
                                        @if ($User->Status)
                                            <input class="form-check-input" value="0" type="radio"
                                                id="Status" name="Status">
                                        @else
                                            <input class="form-check-input" value="0" type="radio"
                                                id="Status" name="Status" checked>
                                        @endif
                                        <label class="form-check-label" for="autoSizingCheck">Non</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Modifier') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
