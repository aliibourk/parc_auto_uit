@extends('layouts.app')

@section('title')
Users 
@endsection
   
@section('content')

<center>
    <h1 class="mt-4">
        <span class="text-uppercase">liste de Staff</span>
    </h1>
    <br>
    {{-- <div>
        <h5>{{ $Users->count() }} User(s)</h5>
    </div> --}}
</center>

<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">
            <a href="/Users">Users</a> 
        </li>
    </ol>
</div>

<center>
    <a class="btn btn-primary" style="font-size: 20px" href="{{ route('add_users')}}"><i class="fas fa-plus-square" ></i><span> Ajouter une Staff</a>
</center>
<br>
<br>

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead class="text-primary">
    <tr>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>#</center></td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">NOM ET PRENOM</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase">EMAIL</td>  
        <td style="border:none;white-space: nowrap" class="text-uppercase">ROLE</td>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>STATUS</center></td>  
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>ACTION</center></td>
    </tr>
</thead>
<tbody id="myTable">   
 
    @php
        $i = 1
    @endphp
    @foreach ($Users as $User)
    <tr>
        <td>
            <center>
                <a>{{ $i++ }}</a>    
            </center>  
        </td>     
        <td>
            {{$User->name}}
        </td>
        <td>
            {{$User->email}}
        </td>
        <td>
            <label class="badge badge-success">{{ $User->getRoleNames()[0] }}</label>
        </td>
        <td> 
            <center>
                @if ($User->Status)
                   <span class="badge badge-pill  badge-success">Active</span>
                @else
                    <span class="badge badge-pill  badge-danger">suspendu</span>
                @endif
            </center>               
        </td>     
        <td>
            <center>
                    
                <a href="{{ route('Users.edit' , $User->id ),'/edit' }}" class="Edit" title="Modifier" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                <a href="#" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $User->id }}" data-whatever="{{ $User->id }}" ><i class="fas fa-trash-alt fa-lg"></i></a>

                <div class="modal fade" id="exampleModal-{{ $User->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <form method="POST" action="{{ route('Users.destroy' , $User->id ) }}" role="form">
                        @csrf
                        @method('DELETE')
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                            <input type="hidden" name="dele" value="{{ $User->id }}">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <h3> voulez vous supprimer ce User : {{ $User->name}}</h3>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                            </div>
                        </div>
                        </div>
                    </form>
                </div>

            </center> 
        </td>
    </tr> 

    @endforeach     


</tbody>
</table>
        
</div>
</div>        
</div>

@endsection
