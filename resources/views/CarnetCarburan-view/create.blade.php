@extends('layouts.app')

@section('title')
Ajouter Carnet
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER Carnet</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('CarnetCarburant.index') }}">Carnet Carburant &nbsp;</a><a href="/CarnetCarburant/create">/ Ajouter</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau Carburant</h3>
            </div>
            <div class="card-body">

<form id="add-form" method="post" action="{{ route('CarnetCarburant.store') }}" role="form">
    <div class="messages"></div>
    @csrf
    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                    <select id="vehicule_id" name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                        <option value="" selected disabled>selectionner une vehicule</option>
                        @foreach ($Vehicule_libre as $vehicule)
                            @if (old('vehicule_id') == $vehicule->id)
                                <option style="background-color:rgba(112, 221, 97, 0.644)" value="{{ $vehicule->id }}" selected>{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                            @else
                                <option style="background-color:rgba(112, 221, 97, 0.644)" value="{{ $vehicule->id }}">{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                            @endif
                        @endforeach
                    </select>
                    <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="debut">Debut :<span style="color: red"> *</span></label>
                    <input id="debut" type="Number"  value="{{ old('debut') }}" name="debut" class="form-control @if($errors->get('debut')) border border-danger @endif" placeholder="debut *" required="required" data-error="debut est obligatoire.">
                    <span style="color: red">@error('debut'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="fin">Fin :<span style="color: red"> *</span></label>
                    <input id="fin" type="Number"  value="{{ old('fin') }}" name="fin" class="form-control @if($errors->get('fin')) border border-danger @endif" placeholder="fin *" required="required" data-error="Date_creation est obligatoire.">
                    <span style="color: red">@error('fin'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
        <div class="row">
        
                                
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
        
    </div>
    
</form>


</div>
</div>
</div>
</div>

@endsection
