@extends('layouts.app')

@section('title')
Edit Carnet 
@endsection

@section('content')

<center>
    <h1 class="mt-4"><span class="text-uppercase">MODIFIER Carnet</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/CarnetCarburant">Carnet Carburant &nbsp;</a><a href="{{ route('CarnetCarburant.edit', $CarnetCarburant->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Modifier Carnet Carburant : Referance {{ $CarnetCarburant->id }}</h3>
            </div>
            <div class="card-body">

<form id="add-form" method="post" action="{{ route('CarnetCarburant.update',$CarnetCarburant->id) }}" role="form">
    @csrf
    @method('PUT')
    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                    <select id="vehicule_id" disabled name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                        <option selected disabled>selectionner une vehicule</option>
                        <option selected>{{ $vehicule->type }} :{{ $vehicule->marque }} {{ $vehicule->immatriculation }}</option>
                    </select>
                    <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="debut">Debut :<span style="color: red"> *</span></label>
                    <input id="debut" type="Number"  value="{{ $CarnetCarburant->debut }}" name="debut" class="form-control @if($errors->get('debut')) border border-danger @endif" placeholder="debut *" required="required" data-error="debut est obligatoire.">
                    <span style="color: red">@error('debut'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="fin">Fin :<span style="color: red"> *</span></label>
                    <input id="fin" type="Number"  value="{{ $CarnetCarburant->fin }}" name="fin" class="form-control @if($errors->get('fin')) border border-danger @endif" placeholder="fin *" required="required" data-error="Date_creation est obligatoire.">
                    <span style="color: red">@error('fin'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
        <div class="row">
        
                                
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Modifier">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
        
    </div>
    
</form>

</div>
</div>
</div>
</div>

@endsection
