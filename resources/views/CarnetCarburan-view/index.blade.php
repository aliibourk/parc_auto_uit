@extends('layouts.app')

@section('title')
    index Carnet 
@endsection

@section('content')
<center>
    <h1 class="mt-4">
        <span class="text-uppercase">
            @if ($archive == true)
                <span style="color: RGB(220, 53, 69)">{{ $TypeList }}</span>
            @else
                <span>{{ $TypeList }}</span>
            @endif
        </span>
    </h1>
    <div>
        <h5>{{ $Carnet->count() }} Carnet(s)</h5>
    </div>
</center>


    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">
                <a href="/CarnetCarburant">Carnet Carburant &nbsp;</a> 
                @if ($archive == true)
                    <a href="/CarnetCarburant/archive">/ Archive</a>
                @endif
            </li>
        </ol>
    </div>
    
    @if ($archive == false)

@can('create carnet')

    <center>
        <a class="btn btn-primary" style="font-size: 20px" href="{{ route('CarnetCarburant.create') }} "><i
                class="fas fa-plus-square"></i><span> Ajouter Nouveau Carnet Carburant</a>
    </center>
@endcan
    @endif
    <BR>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Date creation</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase"><center>Vehicule</center></td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">debut</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Fin</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">page total</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">page consommé</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase"><center>Validité</center></td>
                                {{-- <td style="border:none;white-space: nowrap" class="text-uppercase">Action</td> --}}
                            </tr>
                        </thead>
                        <tbody id="myTable">

                            @forelse($Carnet as $CC)
                                <tr>
                                    <td>

                                        @if ($CC->Date_creation == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $CC->Date_creation }}
                                        @endif

                                    </td>
                                    <td  style="white-space: nowrap">
                                        @if ($CC->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($CC->vehicule_id == $vehicule->id)

                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif

                                                    {{ $vehicule->type }} : {{ $vehicule->marque }}
                                                    {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <center>
                                            @if ($CC->debut == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $CC->debut }}
                                            @endif
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            @if ($CC->fin == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $CC->fin }}
                                            @endif
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            @if ($CC->Npage_total == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $CC->Npage_total }}
                                            @endif
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            @if ($CC->Npage_consomed >= $CC->Npage_total)
                                                <span style="color: red">{{ $CC->Npage_consomed }}</span>
                                            @else
                                                {{ $CC->Npage_consomed }}
                                            @endif
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            @if ($CC->Npage_total > $CC->Npage_consomed)
                                                <span class="badge badge-pill badge-success">Valide</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Expiré</span>
                                            @endif
                                        </center>
                                    </td>
                                    {{-- <td>
                                        <center>
                                            @if ($archive == true)
                                            <form method="POST" class="fm-inline" action="{{ url('CarnetCarburant/'.$CC->id.'/restore') }}">
                                                @csrf
                                                @method('PATCH')
                                                <input type="submit" value="Restaurer" class="btn btn-outline-info btn-sm">
                                            </form>
                                            @else
                                            <a href="{{ route('CarnetCarburant.edit', $CC->id), '/edit' }}" class="Edit"
                                                title="Modifier" data-toggle="tooltip"><i
                                                    class="fas fa-edit fa-lg"></i></i></a>
                                            @endif
                                        </center>
                                    </td> --}}
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Carnet Carburant</center>
                                            </strong> </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if ($archive == false)
    <div class="badge badge-success text-wrap" style="width: 19rem;">
        <a href="/CarnetCarburant/archive" class="text-light"><i class="fas fa-archive"></i> <small>Voir l'archive des Carnet Carburant --></small></a>
    </div>
@else
    <div class="badge badge-danger text-wrap" style="width: 19rem;">
        <a href="/CarnetCarburant" class="text-light"><i class="fas fa-book-open"></i> <small>Voir les Carnets Carburant actual--></small></a>
    </div>
@endif

@endsection
