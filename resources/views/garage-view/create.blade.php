@extends('layouts.app')

@section('title')
Ajouter Garage 
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER Garage</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('Garages.index') }}">Garages &nbsp;</a><a href="/Garages/create">/ Ajouter</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau Garage</h3>
            </div>
            <div class="card-body">

<form id="add-form" method="post" action="{{ route('Garages.store') }}" role="form">
    <div class="messages"></div>
    @csrf
    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NomGarage">NomGarage :<span style="color: red"> *</span></label>
                    <input id="NomGarage" type="text" value="{{ old('NomGarage') }}" name="NomGarage" class="form-control @if($errors->get('NomGarage')) border border-danger @endif" placeholder="NomGarage *" required="required" data-error="NomGarage est obligatoire.">
                     <span style="color: red">@error('NomGarage'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Address">Address :</label>
                    <input id="Address" type="text" value="{{ old('Address') }}" name="Address" class="form-control @if($errors->get('Address')) border border-danger @endif" placeholder="Address">
                     <span style="color: red">@error('debut'){{$message}}@enderror</span>
                </div>
                <span style="color: red">@error('Address'){{$message}}@enderror</span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Telephone">Telephone :</label>
                    <input id="Telephone" type="number" value="{{ old('Telephone') }}" name="Telephone" class="form-control @if($errors->get('Telephone')) border border-danger @endif" placeholder="Telephone : 0...">
                    <span style="color: red">@error('Telephone'){{$message}}@enderror</span>
                </div>
            </div>
                                
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
        
    </div>
    
</form>


</div>
</div>
</div>
</div>
@endsection
