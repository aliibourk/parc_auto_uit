@extends('layouts.app')

@section('title')
index Garages 
@endsection
   
@section('content')

<center>
    <h1 class="mt-4"><span class="text-uppercase">LISTE DES garages</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Garages">Garages</a> </li>
    </ol>
</div>

@can('create garage')
    <center>
        <a class="btn btn-primary" style="font-size: 20px" href="{{ route('Garages.create') }} "><i class="fas fa-plus-square" ></i><span> Ajouter Nouveau Garage</a>
    </center>
@endcan
    <BR>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead class="text-primary">
        <tr>
            <td style="border:none;white-space: nowrap" class="text-uppercase">#</td>
            <td style="border:none;white-space: nowrap" class="text-uppercase">Garage</td>
            <td style="border:none;white-space: nowrap" class="text-uppercase">Address</td>
            <td style="border:none;white-space: nowrap" class="text-uppercase">Telephone</td>
            @can('edit garage')
                <td style="border:none;white-space: nowrap" class="text-uppercase"><center>Action</center></td>
            @endcan
        </tr>
    </thead>
    <tbody id="myTable">   
        @forelse($garages as $Garage)
    <tr>
        <td>
            {{$Garage->id}}
        </td>
        <td>
                @if ($Garage->NomGarage == null)
                    <span style="color: red">---</span>
                @else
                    {{$Garage->NomGarage}}
                @endif
        </td>
        <td>
                @if ($Garage->Address == null)
                    <span style="color: red">---</span>
                @else
                    {{$Garage->Address}}
                @endif
        </td>
        <td>
                @if ($Garage->Telephone == null)
                    <span style="color: red">---</span>
                @else
                    {{$Garage->Telephone}}
                @endif
        </td>  
        @can('edit garage')
        <td>
            <center>
                <a href="{{ route('Garages.edit' , $Garage->id ),'/edit' }}" class="Edit" title="Modifier" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                {{-- <a href="#" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $Garage->id }}" data-whatever="{{ $Garage->id }}" ><i class="fas fa-trash-alt fa-lg"></i></a>      

                <div class="modal fade" id="exampleModal-{{ $Garage->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <form method="POST" action="{{ route('Garages.destroy' , $Garage->id ) }}" role="form">
                        @csrf
                        @method('DELETE')
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                            <input type="hidden" name="dele" value="{{ $Garage->id }}">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <h3> voulez vous supprimer le Garage : {{ $Garage->NomGarage }}</h3>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                            </div>
                        </div>
                        </div>
                    </form>
                </div> --}}
            </center>                         
        </td>
        @endcan
    </tr> 
    @empty  
    <tr>
        <td colspan="5">
            <div class="card-header"><strong style="color: red"> <center>Aucune Garage</center> </strong> </div>
        </td>
    </tr>
    @endforelse 

</tbody>
</table>

</div>
</div>
</div>        
</div>
@endsection
