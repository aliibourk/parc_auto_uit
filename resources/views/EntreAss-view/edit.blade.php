@extends('layouts.app')

@section('title')
Edit l'entreprise
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase"> MODIFIER entreprise d'Assaurance</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Entreprise_assaurance">Entreprises &nbsp;</a><a
                href="{{ route('Entreprise_assaurance.edit', $EntreAss->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">
<div class="card-header"><h3>Modifier : {{$EntreAss->NomAssaurance}}</h3>  </div>
<div class="card-body"> 

<form id="add-form" method="post" action="{{ route('Entreprise_assaurance.update', $EntreAss->id ) }}" role="form">
    @csrf
    @method('PUT')
    <div class="messages"></div>
        <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NomAssaurance">Nom d'entreprise :<span style="color: red"> *</span></label>
                        <input id="NomAssaurance" type="text" value="{{ $EntreAss->NomAssaurance }}" disabled class="form-control" placeholder="NomAssaurance" required="required" data-error="NomAssaurance est obligatoire.">
                        <span style="color: red">@error('NomAssaurance'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Address">Address :</label>
                    <input id="Address" type="text" value="{{ $EntreAss->Address }}" name="Address" class="form-control @if($errors->get('Address')) border border-danger @endif" placeholder="Address">
                    <span style="color: red">@error('Address'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Telephone">Telephone :</label>
                    <input id="Telephone" type="text" value="{{ $EntreAss->Telephone }}" name="Telephone" class="form-control @if($errors->get('Telephone')) border border-danger @endif" placeholder="Telephone">
                    <span style="color: red">@error('Telephone'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
 
        <div class="col-md-12">
            <input type="submit" class="btn btn-primary btn-send" value="Modifier">
        </div>
                            
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">             
                            <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
    </div>
</form>

</div>
</div>
</div>
</div>
@endsection
