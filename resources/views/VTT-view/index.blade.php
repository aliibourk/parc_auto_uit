@extends('layouts.app')

@section('title')
    index VTT
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase">Liste des Bons Transport Terrestre</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/VTT">VTT /</a> </li>
        </ol>
    </div>
@can('create vtt')
    <center>
        <a class="btn btn-primary" style="font-size: 20px" href="{{ route('VTT.create') }} "><i
                class="fas fa-plus-square"></i><span> Ajouter Bon de Transport</a>
    </center>
@endcan 
<BR>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Date</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">N Bon</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Bénéficiaire</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">De</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">A</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Montant</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Transporteur</td>
                                @canany(['edit vtt', 'delete vtt'])
                                    <td style="border:none;white-space: nowrap" class="text-uppercase">Action</td>
                                @endcan
                            </tr>
                        </thead>
                        <tbody id="myTable">

                            @forelse($VTTs as $VTT)
                                <tr>
                                    <td>
                                        @if ($VTT->date == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->date }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->N_Bon == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->N_Bon }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->Beneficiare == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->Beneficiare }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->From == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->From }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->To == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->To }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->Montant == 0)
                                            <span style="color: red">0</span>
                                        @else
                                            {{ $VTT->Montant }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($VTT->Transporteur == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $VTT->Transporteur }}
                                        @endif
                                    </td>
                                    <td>
                                        <center>
                                            @can('edit vtt')
                                                <a href="{{ route('VTT.edit', $VTT->id), '/edit' }}" class="Edit" title="Modifier" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                                            @endcan

                                            @can('delete vtt')
                                            <a href="#" class="delete" title="Supprimer" data-toggle="modal"
                                                data-target="#exampleModal-{{ $VTT->id }}"
                                                data-whatever="{{ $VTT->id }}"><i
                                                    class="fas fa-trash-alt fa-lg"></i></a>

                                            <div class="modal fade" id="exampleModal-{{ $VTT->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form method="POST" action="{{ route('VTT.destroy', $VTT->id) }}"
                                                    role="form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">confirmation
                                                                </h5>
                                                                <input type="hidden" name="dele"
                                                                    value="{{ $VTT->id }}">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3> voulez vous supprimer ce Bon</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-danger btn-send"
                                                                    value="Supprimer">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endcan

                                        </center>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Bon de Transport</center>
                                            </strong> </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
