@extends('layouts.app')

@section('title')
Ajouter VTT
@endsection
 
@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER VTT</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('VTT.index') }}">VTT &nbsp;</a><a href="/VTT/create">/ Ajouter</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau VTT</h3>
            </div>
            <div class="card-body">

<form id="add-form" method="post" action="{{ route('VTT.store') }}" role="form">
    <div class="messages"></div>
    @csrf
    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="N_Bon">N Bon :<span style="color: red"> *</span></label>
                    <input id="N_Bon" type="text"  value="{{ old('N_Bon') }}" name="N_Bon" class="form-control @if($errors->get('N_Bon')) border border-danger @endif" placeholder="DB N°" required="required" data-error="N_Bon est obligatoire.">
                    <span style="color: red">@error('N_Bon'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Transporteur">Transporteur :</label>
                    <input id="Transporteur" type="text"  value="{{ old('Transporteur') }}" name="Transporteur" class="form-control @if($errors->get('Transporteur')) border border-danger @endif" placeholder="Transporteur">
                    <span style="color: red">@error('Transporteur'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Beneficiare">Bénéficiaire :<span style="color: red"> *</span></label>
                    <input id="Beneficiare" type="text"  value="{{ old('Beneficiare') }}" name="Beneficiare" class="form-control @if($errors->get('Beneficiare')) border border-danger @endif" placeholder="Beneficiare" required="required" data-error="Beneficiare est obligatoire.">
                    <span style="color: red">@error('Beneficiare'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="From">De :</label>
                    <input id="From" type="text"  value="{{ old('From') }}" name="From" class="form-control @if($errors->get('From')) border border-danger @endif" placeholder="De ">
                    <span style="color: red">@error('From'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="To">A :</label>
                    <input id="To" type="text"  value="{{ old('To') }}" name="To" class="form-control @if($errors->get('To')) border border-danger @endif" placeholder="A">
                    <span style="color: red">@error('To'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Montant">Montant :<span style="color: red"> *</span></label>
                    <input id="Montant" type="Number"  value="{{ old('Montant') }}" name="Montant" class="form-control @if($errors->get('Montant')) border border-danger @endif" placeholder="Montant" required="required" data-error="Montant est obligatoire.">
                    <span style="color: red">@error('Montant'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
        <div class="row">
        
                                
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
        
    </div>
    
</form>


</div>
</div>
</div>
</div>

@endsection
