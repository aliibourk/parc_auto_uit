@extends('layouts.app')

@section('title')
    index Vignette
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase">{{ __('Liste des Vignette') }}</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/Vignette">Vignettes</a> </li>
        </ol>
    </div>
@can('create vignette')
<center>
    <a class="btn btn-primary" style="font-size: 20px" href="{{ route('Vignette.create') }} "><i
            class="fas fa-plus-square"></i><span> Ajouter une Vignette</a>
</center>
@endcan 
<br>
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">#</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Vehicule</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase"><center>Date Validite Vignette</center></td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Status</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Date fin</td>
                                @can('edit vignette')
                                    <td style="border:none;white-space: nowrap" class="text-uppercase"><center>Action</center> </td>
                                @endcan
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @forelse($Vignettes as $Vignette)
                                <tr>
                                    <td>
                                        <a>{{ $Vignette->id }}</a>
                                    </td>
                                    <td>
                                        @if ($Vignette->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($Vignette->vehicule_id == $vehicule->id)

                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif
                                                    {{ $vehicule->type }} : {{ $vehicule->marque }}
                                                    {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <center>
                                        @if ($Vignette->Date_Validite == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Vignette->Date_Validite }}
                                        @endif
                                    </center>
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if (\Carbon\Carbon::parse($Vignette->Date_Validite) < now())
                                            <span class="badge badge-pill  badge-danger">Expiré</span>
                                        @else
                                            <span class="badge badge-pill  badge-success">Valide</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Vignette->Date_Validite == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @if (\Carbon\Carbon::parse($Vignette->Date_Validite) < now())
                                                <span
                                                    class="badge badge-pill  badge-danger">{{ \Carbon\Carbon::parse($Vignette->Date_Validite)->diffForHumans() }}</span>
                                            @else
                                                <span
                                                    class="badge badge-pill  badge-success">{{ \Carbon\Carbon::parse($Vignette->Date_Validite)->diffForHumans() }}</span>
                                            @endif
                                        @endif
                                    </td>

                                    @can('edit vignette')
                                    <td>
                                        <center><a href="{{ route('Vignette.edit', $Vignette->id), '/edit' }}" class="Edit"
                                            title="Modifier" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></a></center>
                                        {{-- <a href="#" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $Vignette->id }}" data-whatever="{{ $Vignette->id }}" ><i class="fas fa-trash-alt fa-lg"></i></a> --}}

                                        {{-- <div class="modal fade" id="exampleModal-{{ $Vignette->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <form method="POST" action="{{ route('Vignette.destroy' , $Vignette->id ) }}" role="form">
                        @csrf
                        @method('DELETE')
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                            <input type="hidden" name="dele" value="{{ $Vignette->id }}">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <h4>
                                    @foreach ($liste_vehicule as $vehicule)
                                        @if ($Vignette->vehicule_id == $vehicule->id)
                                            voulez vous supprimer la Vignette de {{ $vehicule->type}}  :
                                            {{ $vehicule->marque}} {{ $vehicule->immatriculation }}
                                        @endif
                                    @endforeach
                                </h4>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                            </div>
                        </div>
                        </div>
                    </form>
                </div> --}}
                                </td>
                                @endcan 
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Vignette</center>
                                            </strong> </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endsection
