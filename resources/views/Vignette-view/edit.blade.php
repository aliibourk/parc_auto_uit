@extends('layouts.app')

@section('title')
Edit Vignette
@endsection

@section('content')
  

<center>
    <h1 class="mt-4"><span class="text-uppercase"> MODIFIER Vignette</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Vignette">Vignettes &nbsp;</a><a href="{{ route('Vignette.edit', $Vignette->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Modifier Vignette : Referance {{ $Vignette->id }}</h3>
            </div>
            <div class="card-body">
                 

    <form id="add-form" method="POST" action="{{ route('Vignette.update' , $Vignette->id ) }}" role="form">
      @csrf
      @method('PUT')
      <div class="messages"></div>
      <div class="controls">

          <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                    <select id="vehicule_id" disabled class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                        <option selected>{{ $MyVehicule->type}} : {{ $MyVehicule->marque}} {{ $MyVehicule->immatriculation }}</option>        
                    </select>
                </div>
            </div>
          </div>

        <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="Date_Validite">Date Validité :<span style="color: red"> *</span></label>
                  <input id="Date_Validite" name="Date_Validite" type="date" value="{{ $Vignette->Date_Validite }}" class="form-control @if($errors->get('Date_Validite')) border border-danger @endif"  required="required">
                  <span style="color: red">@error('Date_Validite'){{$message}}@enderror</span>
              </div>
          </div>
        </div>

     
          <div class="row">     
              <div class="col-md-12">
                  <input type="submit" class="btn btn-primary btn-send" value="Modifier">
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <p class="text-muted">
                      <strong><span style="color: red">* est obligatoire.</span></strong>
              </div>
          </div>
      </div>

  </form>
</div>

        </div>
      </div>
</div>
@endsection
