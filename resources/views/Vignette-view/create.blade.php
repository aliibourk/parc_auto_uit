@extends('layouts.app')

@section('title')
Ajouter Vignette
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase"> AJOUTER Vignette </span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('Vignette.index') }}">Vignettes &nbsp;</a><a href="/Vignette/create">/ Ajouter</a>
        </li>
        {{-- <button class="btn btn-danger"><a href="/Vehicules_to_excel">Exporter Excel</a> </button>
    <button class="btn btn-info"><a href="/Vehicules_to_csv">Exporter CSV</a> </button> --}}
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter une Vignette</h3>
            </div>
            <div class="card-body">

                      <form id="add-form" method="post" action="{{ route('Vignette.store') }}" role="form">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                          
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                        <select id="vehicule_id" value="{{ old('vehicule_id') }}" name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                                            <option value="" selected disabled>selectionner une vehicule</option>
                                            @foreach ($Vehicule_libre as $vehicule)
                                                <option style="background-color:rgba(112, 221, 97, 0.644)"x value="{{ $vehicule->id }}" >{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>                               
                                            @endforeach
                                        </select>
                                        <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Date_Validite">Date Validité :<span style="color: red"> *</span></label>
                                        <input id="Date_Validite" type="date" name="Date_Validite"  value="{{ old('Date_Validite') }}" class="form-control @if($errors->get('Date_Validite')) border border-danger @endif" required="required" >
                                        <span style="color: red">@error('Date_Validite'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                             
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted">
                                        <strong><span style="color: red">* est obligatoire.</span></strong>
                                </div>
                            </div>
                        </div>
    
                    </form>
            </div>
        </div>
      </div>
</div>
@endsection
