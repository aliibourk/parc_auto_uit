@extends('layouts.app')

@section('title')
index chauffeures 
@endsection
   
@section('content')

<center>
    <h1 class="mt-4">
        <span class="text-uppercase">
            @if ($archive == true)
                <span style="color: RGB(220, 53, 69)">{{ $TypeList }}</span>
            @else
                <span>{{ $TypeList }}</span>
            @endif
        </span>
    </h1>
    <div>
        <h5>{{ $Chauffeures->count() }} Chauffeure(s)</h5>
    </div>
</center>

<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">
            <a href="/chaufeures">Chauffeures &nbsp;</a> 
            @if ($archive == true)
                <a href="/chaufeures/archive">/ Archive</a>
            @endif
        </li>
    </ol>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead class="text-primary">
    <tr>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>CIN</center></td>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>NOM</center></td>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>PRENOM</center></td>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>TELEPHONE</center></td>
        <td style="border:none;white-space: nowrap" class="text-uppercase"><center>DISPONIBLE</center></td> 
        @if ($archive == true)
            @can('restore chauffeure')
                <td  class="text-right text-uppercase" style="border:none;white-space: nowrap">ACTION</td>
            @endcan
        @else
            @canany(['delete chauffeure', 'edit chauffeure'])
                <td  class="text-right text-uppercase" style="border:none;white-space: nowrap">ACTION</td>
            @endcan
        @endif  
    </tr>
</thead>
<tbody id="myTable">   
 
    @forelse($Chauffeures as $chauffeure)
    <tr>
        <td>
            <center>
                {{-- <a href="{{ route('chaufeures.show', $chauffeure->id ) }}">{{$chauffeure->CIN}}</a>     --}}
                <a>{{$chauffeure->CIN}}</a>    
            </center>  
        </td>     
        <td>
            <center>
                 @if ($chauffeure->nom == null)
                    <span style="color: red">---</span>
                @endif
                    {{$chauffeure->nom}}
            </center>
        </td>
        <td>
            <center>
                @if ($chauffeure->prenom == null)
                    <span style="color: red">---</span>
                @endif
                    {{$chauffeure->prenom}}
            </center>
        </td>
        <td>
            <center>
                @if ($chauffeure->telephone == null)
                    <span style="color: red">---</span>
                @endif
                    {{$chauffeure->telephone}}
            </center>
        </td>
        <td> 
            <center>
                @if ($chauffeure->disposible)
                   <span class="badge badge-pill  badge-success">Active</span>
                @else
                    <span class="badge badge-pill  badge-danger">Inactive</span>
                @endif
            </center>               
        </td>     
        <td>
            <center>
                @if ($archive == true)
                
                    @can('restore chauffeure')
                    <form method="POST" class="fm-inline" action="{{ url('chaufeures/'.$chauffeure->id.'/restore') }}">
                        @csrf
                        @method('PATCH')

                        <input type="submit" value="Restaurer" class="btn btn-outline-info btn-sm">
                    </form>
                    @endcan
                @else
                @can('edit chauffeure')
                    <a href="{{ route('chaufeures.edit' , $chauffeure->id ),'/edit' }}" class="Edit" title="Modifier" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                @endcan
                @can('delete chauffeure')
                    <a href="#" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $chauffeure->id }}" data-whatever="{{ $chauffeure->id }}" ><i class="fas fa-trash-alt fa-lg"></i></a>      

                <div class="modal fade" id="exampleModal-{{ $chauffeure->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <form method="POST" action="{{ route('chaufeures.destroy' , $chauffeure->id ) }}" role="form">
                        @csrf
                        @method('DELETE')
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                            <input type="hidden" name="dele" value="{{ $chauffeure->id }}">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <h3> voulez vous supprimer ce chauffeure : {{ $chauffeure->prenom}} {{ $chauffeure->nom}}</h3>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
                @endcan

                @endif

            </center> 
        </td>
    </tr> 
    @empty  
    <tr>
        <td colspan="6">
            <div class="card-header"><strong style="color: red"> <center>Aucune Chauffeure</center> </strong> </div>
        </td>
    </tr> 
    @endforelse     


</tbody>
</table>
        
</div>
</div>        
</div>

@if ($archive == false)
    <div class="badge badge-success text-wrap" style="width: 16rem;">
        <a href="/chaufeures/archive" class="text-light"><i class="fas fa-archive"></i> <small>Voir l'archive des Chaufeures --></small></a>
    </div>
@else
    <div class="badge badge-danger text-wrap" style="width: 16rem;">
        <a href="/chaufeures" class="text-light"><i class="fas fa-user"></i> <small>Voir les chaufeures actual--></small></a>
    </div>
@endif

@endsection
