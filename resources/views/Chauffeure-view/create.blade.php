@extends('layouts.app')

@section('title')
Ajouter Chauffeure
@endsection

@section('content')
  
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER Chauffeure</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('chaufeures.index') }}">Chauffeures &nbsp;</a><a href="/chaufeures/create">/ Ajouter</a>
        </li>
        {{-- <button class="btn btn-danger"><a href="/Vehicules_to_excel">Exporter Excel</a> </button>
    <button class="btn btn-info"><a href="/Vehicules_to_csv">Exporter CSV</a> </button> --}}
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau Chauffeure</h3>
            </div>
            <div class="card-body">


    <form id="add-form" method="post" action="{{ route('chaufeures.store') }}" role="form">
    @csrf
    <div class="messages"></div>
        <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="CIN">CIN :<span style="color: red"> *</span></label>
                        <input id="CIN" type="text" value="{{ old('CIN') }}" name="CIN" class="form-control @if($errors->get('CIN')) border border-danger @endif" placeholder="CIN" required="required" data-error="CIN est obligatoire.">
                        <span style="color: red">@error('CIN'){{$message}}@enderror</span>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="prenom">Prenom :<span style="color: red"> *</span></label>
                    <input id="prenom" type="text" value="{{ old('prenom') }}" name="prenom" class="form-control @if($errors->get('prenom')) border border-danger @endif" placeholder="Prenom" required="required" data-error="prenom est obligatoire.">
                    <span style="color: red">@error('prenom'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nom">Nom :<span style="color: red"> *</span></label>
                    <input id="nom" type="text" value="{{ old('nom') }}" name="nom" class="form-control @if($errors->get('nom')) border border-danger @endif" placeholder="nom" required="required" data-error="nom est obligatoire.">
                    <span style="color: red">@error('nom'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="telephone">Telephone :</label>
                    <input id="telephone" value="{{ old('telephone') }}" type="number" name="telephone" class="form-control @if($errors->get('telephone')) border border-danger @endif" placeholder="Telephone : 06..." >
                    <span style="color: red">@error('telephone'){{$message}}@enderror</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="disposible">Disponible</label>
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="form-check mb-2">
                            <input class="form-check-input" value="1" type="radio" id="disposible" name="disposible" checked>
                            <label class="form-check-label" for="disposible">Oui</label>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-check mb-2">
                            <input class="form-check-input" value="0" type="radio" id="disposible" name="disposible">
                            <label class="form-check-label" for="disposible">Non</label>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
         
        </div>
        {{-- <div class="mb-3 form-check-inline">
            <label class="form-check-label" for="exampleCheck1">disponible</label>
        </div>
        <div class="mb-3 form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            <label class="form-check-label" for="exampleCheck1">Oui</label>
        </div>
        <div class="mb-3 form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
            <label class="form-check-label" for="exampleCheck1">Non</label>
        </div> --}}
 
                      
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
            </div>
                            
        
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">             
                            <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
    </div>
    </form>
</div>
</div>
</div>
</div>
@endsection
