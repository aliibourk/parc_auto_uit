@extends('layouts.app')

@section('title')
    Edit Chauffeure
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase">MODIFIER CHAUFFEURE</span> </h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/chaufeures">Chauffeures &nbsp;</a><a
                    href="{{ route('chaufeures.edit', $Chauffeure->id), '/edit' }}">/ Modifier</a>
            </li>
        </ol>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Modifier : <span class="text-uppercase">{{ $Chauffeure->prenom }} {{ $Chauffeure->nom }}</span>
                    </h3>
                </div>
                <div class="card-body">
                    <form id="add-form" method="post" action="{{ route('chaufeures.update', $Chauffeure->id) }}"
                        role="form">
                        @csrf
                        @method('PUT')
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="CIN">CIN :<span style="color: red"> *</span></label>
                                        <input id="CIN" type="text" value="{{ $Chauffeure->CIN }}" name="CIN"
                                            class="form-control" placeholder="CIN" required="required"
                                            data-error="CIN est obligatoire." disabled>
                                        <span style="color: red">@error('CIN'){{ $message }}@enderror</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="prenom">Prenom :<span style="color: red"> *</span></label>
                                            <input id="prenom" disabled type="text" value="{{ $Chauffeure->prenom }}"
                                                name="prenom" class="form-control" placeholder="Prenom" required="required"
                                                data-error="prenom est obligatoire.">
                                            <span style="color: red">@error('prenom'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nom">Nom :<span style="color: red"> *</span></label>
                                                <input id="nom" disabled type="text" value="{{ $Chauffeure->nom }}" name="nom"
                                                    class="form-control" placeholder="nom" required="required"
                                                    data-error="nom est obligatoire.">
                                                <span style="color: red">@error('nom'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="telephone">Telephone :</label>
                                                    <input id="telephone" type="number" value="{{ $Chauffeure->telephone }}"
                                                        name="telephone" class="form-control @if ($errors->get('telephone')) border border-danger @endif" placeholder="Telephone : 06..." >
                                                    <span style="color: red">@error('telephone'){{ $message }}@enderror</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="disposible">Disponible :</label>
                                                        <div class="form-row align-items-center">

                                                            <div class="col-auto">
                                                                <div class="form-check mb-2">
                                                                    @if ($Chauffeure->disposible)
                                                                        <input class="form-check-input" value="1" type="radio"
                                                                            id="disposible" name="disposible" checked>
                                                                    @else
                                                                        <input class="form-check-input" value="1" type="radio"
                                                                            id="disposible" name="disposible">
                                                                    @endif
                                                                    <label class="form-check-label" for="autoSizingCheck">Oui</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-auto">
                                                                <div class="form-check mb-2">
                                                                    @if ($Chauffeure->disposible)
                                                                        <input class="form-check-input" value="0" type="radio"
                                                                            id="disposible" name="disposible">
                                                                    @else
                                                                        <input class="form-check-input" value="0" type="radio"
                                                                            id="disposible" name="disposible" checked>
                                                                    @endif
                                                                    <label class="form-check-label" for="autoSizingCheck">Non</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <input type="submit" class="btn btn-primary btn-send" value="Modifier">
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-muted">
                                                        <strong><span style="color: red">* est obligatoire.</span></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                @endsection
