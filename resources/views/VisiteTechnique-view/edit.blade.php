@extends('layouts.app')

@section('title')
    Edit Visite
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase"> MODIFIER Visite Technique</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/VisiteTechnique">Visite Technique &nbsp;</a><a
                    href="{{ route('VisiteTechnique.edit', $Visite->id), '/edit' }}">/ Modifier</a>
            </li>
        </ol>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Modifier Visite : Referance {{ $Visite->id }}</h3>
                </div>
                <div class="card-body">

                    <form id="add-form" method="POST" action="{{ route('VisiteTechnique.update', $Visite->id) }}"
                        role="form">
                        @csrf
                        @method('PUT')
                        <div class="messages"></div>
                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                        <select id="vehicule_id" disabled class="form-control" required="required">
                                            <option selected>{{ $MyVehicule->type }} : {{ $MyVehicule->marque }} {{ $MyVehicule->immatriculation }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chauffeure_id">Chauffeure :<span style="color: red"> *</span></label>
                                        <select id="chauffeure_id" disabled class="form-control" required="required" >
                                            <option selected> {{ $MyChauffeure->nom }} {{ $MyChauffeure->prenom }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="date">Date :<span style="color: red"> *</span></label>
                                                <input id="date" type="date" value="{{ $Visite->date }}" name="date"
                                                    class="form-control @if ($errors->get('date')) border border-danger @endif" required="required" >
                                                <span style="color: red">@error('date'){{ $message }}@enderror</span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="next_Visite">La peroide pour la prochaine Visite :<span
                                                            style="color: red"> *</span></label>
                                                    <div class="input-group mb-3">
                                                        <input id="next_Visite" value="{{ $Visite->next_Visite }}" required
                                                            type="number" name="next_Visite" class="form-control @if ($errors->get('next_Visite')) border border-danger @endif" placeholder="La prochaine Visite">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">Mois</span>
                                                        </div>
                                                    </div>

                                                    <span style="color: red">@error('next_Visite'){{ $message }}@enderror</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="object">Object :</label>
                                                        <div class="input-group mb-3">
                                                            <input id="object" type="text" value="{{ $Visite->object }}" name="object"
                                                                class="form-control @if ($errors->get('object')) border border-danger @endif" placeholder="object ">
                                                            <span style="color: red">@error('object'){{ $message }}@enderror</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="Garage_id">Garage :<span style="color: red"> *</span></label>
                                                            <select id="Garage_id" disabled class="form-control"
                                                                required="required">
                                                                <option selected>{{ $MyGarage->NomGarage }}</option>
                                                            </select>
                                                            <span style="color: red">@error('Garage_id'){{ $message }}@enderror</span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="kelometrage">Kelometrage :</label>
                                                                <div class="input-group mb-3">
                                                                    <input id="kelometrage" type="NUMBER" value="{{ $Visite->kelometrage }}"
                                                                        name="kelometrage" class="form-control @if ($errors->get('kelometrage')) border border-danger @endif" placeholder="kelometrage">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">kilomètre</span>
                                                                    </div>
                                                                    <span
                                                                        style="color: red">@error('kelometrage'){{ $message }}@enderror</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="N_Souche">N Souche :</label>
                                                                    <div class="input-group mb-3">
                                                                        <input id="N_Souche" type="text" name="N_Souche"
                                                                            value="{{ $Visite->N_Souche }}" class="form-control @if ($errors->get('N_Souche')) border
                                                                        border-danger @endif" placeholder="Numero de Souche">
                                                                    </div>
                                                                    <span style="color: red">@error('N_Souche'){{ $message }}@enderror</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="montant_vignette">Montant Carburant :</label>
                                                                        <div class="input-group mb-3">
                                                                            <input id="montant_vignette" value="{{ old('montant_vignette') }}"
                                                                                type="number" name="montant_vignette" class="form-control @if ($errors->get('montant_vignette')) border
                                                                            border-danger @endif" placeholder="montant carburant ">
                                                                            <div class="input-group-append">
                                                                                <span class="input-group-text">DH</span>
                                                                            </div>
                                                                            <span
                                                                                style="color: red">@error('montant_vignette'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="observation">Observation :</label>
                                                                            <textarea id="observation" name="observation" class="form-control @if ($errors->get('observation')) border border-danger @endif" placeholder="observation" rows="4" >{{ $Visite->observation }}</textarea>
                                                                            <span style="color: red">@error('observation'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <input type="submit" class="btn btn-primary btn-send" value="Modifier">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <p class="text-muted">
                                                                                <strong><span style="color: red">* est obligatoire.</span></strong>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endsection
