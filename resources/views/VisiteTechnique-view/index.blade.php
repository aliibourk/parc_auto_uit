@extends('layouts.app')

@section('title')
Visites Technique 
@endsection
   
@section('content')

<center>
    <h1 class="mt-4"><span class="text-uppercase">LISTE DES Visites Technique</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/VisiteTechnique">Visite Technique /</a> </li>
    </ol>
</div>

@can('create visite')
<center>
    <a href="{{ route('VisiteTechnique.create') }}" style="font-size: 20px" class="btn btn-primary"><i class="fas fa-plus-square" ></i> <span>Ajouter Visite Technique</span></a>
</center>
@endcan
<BR>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead class="text-primary">
                        <tr>
    <tr>
        <td style="border:none;white-space: nowrap" class="text-uppercase">#</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Vehicule</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Chauffeure</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Date</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">La Visite Prochaine</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Carburant</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">N Souche</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Object</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Kelometrage</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Benificiere</th>
        <td style="border:none;white-space: nowrap" class="text-uppercase">Observation</th>
        @can('edit visite')
            <td style="border:none;white-space: nowrap" class="text-uppercase">Action</th>
        @endcan
    </tr> 
</thead>
<tbody id="myTable">    
    @forelse($Visites as $Visite)
    <tr>
        <td style="white-space: nowrap">
                <a>{{$Visite->id}}</a>
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->vehicule_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_vehicule as $vehicule)
                        @if ( $Visite->vehicule_id == $vehicule->id)

                            @if ($vehicule->type == "Voiture")
                                <i class="fas fa-car"></i>
                            @else
                                @if ($vehicule->type == "Moto" || $vehicule->type == "Velo")
                                    <i class="fas fa-bicycle"></i>    
                                @else
                                    @if ($vehicule->type == "Bus" || $vehicule->type == "MiniBus")
                                    <i class="fas fa-bus-alt"></i>                   
                                    @endif                
                                @endif
                            @endif
                            {{ $vehicule->marque}} : {{ $vehicule->immatriculation }}
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->chauffeure_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_chauffeure as $chauffeure)
                        @if ( $Visite->chauffeure_id == $chauffeure->id)
                            {{ $chauffeure->prenom }} {{ $chauffeure->nom }}
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
            <center>
                @if ($Visite->date == null)
                    <span style="color: red">---</span>
                @else
                    {{$Visite->date}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->next_Visite == null)
                    <span style="color: red">---</span>
                @else
                    @if (\Carbon\Carbon::parse($Visite->date)->addMonths($Visite->next_Visite) < now())
                        <span class="badge badge-pill  badge-danger">
                            {{ mb_strimwidth(\Carbon\Carbon::parse($Visite->date)->addMonths($Visite->next_Visite),0,10,"")  }}
                        </span> 
                    @else
                        <span class="badge badge-pill  badge-success">
                            {{ mb_strimwidth(\Carbon\Carbon::parse($Visite->date)->addMonths($Visite->next_Visite),0,10,"")  }}                        </span>
                    @endif
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->montant_vignette == null)
                    <span style="color: red">---</span>
                @else
                    {{$Visite->montant_vignette}} DH
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->N_Souche == null)
                    <span style="color: red">---</span>
                @else
                    {{$Visite->N_Souche}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->object == null)
                    <span style="color: red">---</span>
                @else
                    {{$Visite->object}}
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->kelometrage == null)
                    <span style="color: red">---</span>
                @else
                    {{$Visite->kelometrage}} kilo
                @endif
        </td>
        <td style="white-space: nowrap">
                @if ($Visite->Garage_id == null)
                    <span style="color: red">---</span>
                @else
                    @foreach ($liste_garage as $garage)
                        @if ( $garage->id == $Visite->Garage_id)
                            {{ $garage->NomGarage }} 
                        @endif
                    @endforeach
                @endif
        </td>
        <td style="white-space: nowrap">
            @if ($Visite->observation == null)
                <span style="color: red">---</span>
            @else
            {{ mb_strimwidth("$Visite->observation",0,10,"...")  }}
            @endif
        </td>
        @can('edit visite')
        <td style="white-space: nowrap">
            <center>
                <a href="{{ route('VisiteTechnique.edit' , $Visite->id ),'/edit' }}" class="Edit" title="edit" data-toggle="tooltip"><i class="fas fa-edit fa-lg"></i></i></a>
                {{-- <a href="" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $Visite->id }}" data-whatever="{{ $Visite->id }}"><i class="fas fa-trash-alt fa-lg"></i></a> --}}
            
                    {{-- Model --}}
                    {{-- <div class="modal fade" id="exampleModal-{{ $Visite->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <form method="POST" action="{{ route('VisiteTechnique.destroy' , $Visite->id ) }}" role="form">
                            @csrf
                            @method('DELETE')
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                                <input type="hidden" name="dele" value="{{ $Visite->id }}">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <h3> voulez vous supprimer la Visite Technique <br> 
                                        Referance : {{ $Visite->id }}</h3>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                                </div>
                            </div>
                            </div>
                        </form>
                    </div> --}}
            </center>
        </td>
        @endcan
    </tr>

    @empty
    <tr>
        <td colspan="12">
            <div class="card-header"><strong style="color: red"> <center>Aucune Visite Technique</center> </strong> </div>
        </td>
    </tr>
        
    @endforelse  
        
</tbody>
</table>

</div>
</div>        
</div>
</div>

@endsection
