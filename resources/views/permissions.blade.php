@extends('layouts.app')

@section('title')
Permission
@endsection


@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">

    <center>
        <h1 class="mt-4"><span class="text-uppercase">Staff Permission</span></h1>
    </center><br><br>


<br>
@php
    $i = 0;
    $toggleState;
@endphp

<table class="table table-striped table-bordered">
<thead class="thead-warning">
    <tr>
        <th>Permission</th>
        <th class="table-active text-center">Accès</th>
        <th class="table-info text-center">Ajouter</th>
        <th class="table-primary text-center">Editer</th>
        <th class="table-danger text-center">Supprimer</th>
        <th class="table-success text-center">Restaurer</th>
        <th class="bg-danger text-center">Supprimer définitivement</th>
    </tr>
</thead>
<tbody>

    {{-- table-info text-center --}}
@foreach ($Roles as $Role)

<tr>
    <td>Vehicule</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'vehicule'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" name="recu" id="customSwitches-{{ $i }}" 
                    @if ($Role->hasPermissionTo($Permission->name))
                        checked 
                    @endif 
                >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Chauffeure</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'chauffeure'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>VTT</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'vtt'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Assaurance</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'assaurance'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Carburant</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'carburant'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Carnet</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'carnet'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Entreprise Assaurance</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'entreAss'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Entretien</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'entretien'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Garage</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'garage'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Mission</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'mission'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>


<tr>
    <td>Reparation</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'reparation'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Vignette</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'vignette'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>

<tr>
    <td>Visite Technique</td>
    @foreach ($Permissions as $Permission)
        @if(strpos($Permission->name, 'visite'))
            <td class="table-active text-center">
                <input type="checkbox" onclick="event.preventDefault(); document.getElementById('recu-{{ $i }}').click();" 
                name="recu" id="customSwitches-{{ $i }}" 
                @if ($Role->hasPermissionTo($Permission->name))
                    checked 
                @endif 
            >
            <a id="recu-{{ $i }}" href=" {{ route('toogle', [$Role->id, $Permission->id]) }}"></a>
            </td>
            @php
                $i++;
            @endphp
        @endif
    @endforeach
</tr>




    {{-- <tr>
        <td>Chauffeure</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select chauffeure"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create chauffeure"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit chauffeure"><br></td>
        <td class="table-danger text-center"><input type="checkbox" class="role-permission" data-key="delete chauffeure"><br></td>
        <td class="table-success text-center"><input type="checkbox" class="role-permission" data-key="restore chauffeure"><br></td>
    </tr> --}}
    {{-- <tr>
        <td>Assaurance</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select assaurance"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create assaurance"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit assaurance"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Carburant</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select carburant"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create carburant"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit carburant"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Carnet</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select carnet"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create carnet"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit carnet"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Entreprise Assaurance</td>            
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select entreAss"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create entreAss"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit entreAss"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Entretien</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select entretien"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create entretien"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit entretien"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Garage</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select garage"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create garage"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit garage"><br></td>
    </tr> --}}
{{--     
    <tr>
        <td>Mission</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select mission"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create mission"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit mission"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Reparation</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select reparation"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create reparation"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit reparation"><br></td>
    </tr>
     --}}
    {{-- <tr>
        <td>Vignette</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select vignette"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create vignette"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit vignette"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>Visite Technique</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select visite"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create visite"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit visite"><br></td>
    </tr> --}}
    
    {{-- <tr>
        <td>VTT</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="select vtt"><br></td>
        <td class="table-info text-center"><input type="checkbox" class="role-permission" data-key="create vtt"><br></td>
        <td class="table-primary text-center"><input type="checkbox" class="role-permission" data-key="edit vtt"><br></td>
    </tr> --}}

    {{-- <tr>
        <td>Exporte</td>
        <td class="table-active text-center"><input type="checkbox" class="role-permission" data-key="exporte"><br></td>
    </tr> --}}
    
@endforeach

</tbody>
</table>


</div>
</div>
</div>

{{-- <script src="{{ asset('js/jquery-3.6.0.min.js') }}" defer></script>
<script>

        $(document).ready(function(){
            $(document).on('click', '.role-permission', function(){
                var roleName = $(this).attr('data-role');
                var page = $(this).attr('data-page');
                var key = $(this).attr('data-key');
                var val = 0;
                if($(this).is(':checked')){ val = 1; }
                $.ajax({
                    type: "post",
                    url: "{{ url('/permission') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        role: roleName,
                        page: page,
                        key: key,
                        value: val
                    },
                    success: function(){
                        console.log('Assigned - ' + key + ' = ' + val + ' - to ' + roleName);
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            });
        });
    </script> --}}

@endsection
