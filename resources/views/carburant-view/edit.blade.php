@extends('layouts.app')

@section('title')
    Edit Carburant
@endsection

@section('content')

<center> <h1 class="mt-4"><span class="text-uppercase"> MODIFIER Carburant</span></h1> </center>

<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Carburant">Carburants &nbsp;</a><a href="{{ route('Carburant.edit', $Carburant->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Modifier Carburant : Referance {{ $Carburant->id }}</h3>
            </div>
            <div class="card-body">
                <form id="add-form" method="POST" action="{{ route('Carburant.update', $Carburant->id) }}" role="form">
                    @csrf
                    @method('PUT')
                    <div class="messages"></div>
                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                        <select id="vehicule_id" disabled name="vehicule_id" class="form-control">
                                            <option selected>{{ $MyVehicule->marque }} {{ $MyVehicule->immatriculation }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chauffeure_id">Chauffeure :<span style="color: red"> *</span></label>
                                        <select id="chauffeure_id" disabled class="form-control">
                                            <option selected>{{ $MyChauffeure->nom }} {{ $MyChauffeure->prenom }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">Date :<span style="color: red"> *</span></label>
                                        <input id="date" type="date" name="date" value="{{ $Carburant->date }}" class="form-control @if ($errors->get('date')) border border-danger @endif" required="required" >
                                        <span style="color: red">@error('date'){{ $message }}@enderror</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Vignette">Vignette</label>
                                        <input id="Vignette" type="TEXT" name="Vignette" value="{{ $Carburant->Vignette }}" class="form-control @if ($errors->get('Vignette')) border border-danger @endif" placeholder="Vignette">
                                        <span style="color: red">@error('Vignette'){{ $message }}@enderror</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="montant">Montant</label>
                                        <div class="input-group mb-3">
                                            <input id="montant" type="Number" name="montant" value="{{ $Carburant->montant }}" class="form-control @if ($errors->get('montant')) border border-danger @endif" placeholder="montant ">
                                            <div class="input-group-append">
                                                <span class="input-group-text">DH</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <span style="color: red">@error('montant'){{ $message }}@enderror</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Observation</label>
                                        <textarea id="Observation" name="Observation" class="form-control @if ($errors->get('Observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ $Carburant->Observation }}</textarea>
                                        <span style="color: red">@error('Observation'){{ $message }}@enderror</span>
                                    </div>
                                </div>
                            </div>

                           <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary btn-send" value="Modifier">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted">
                                    <strong><span style="color: red">* est obligatoire.</span></strong>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
