@extends('layouts.app')

@section('title')
Ajouter Carburant 
@endsection

@section('content') 

<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER CARBURANT</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Carburant">Carburants &nbsp;</a><a href="/Carburant/create">/ Ajouter</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau Carburant</h3>
            </div>
            <div class="card-body">

                      <form id="add-form" method="post" action="{{ route('Carburant.store') }}" role="form">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                        <select id="vehicule_id" name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                                            <option value="" selected disabled>selectionner une vehicule</option>
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if (old('vehicule_id') == $vehicule->id)
                                                    <option value="{{ $vehicule->id }}" selected>{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                                @else
                                                    <option value="{{ $vehicule->id }}">{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chauffeure_id">Chauffeure :<span style="color: red"> *</span></label>
                                        <select id="chauffeure_id" name="chauffeure_id" class="form-control @if($errors->get('chauffeure_id')) border border-danger @endif" required="required" >
                                            
                                            <option value="" selected disabled>selectionner un chauffeure</option>
                                            @foreach ($liste_chauffeure as $chauffeure)
                                                @if (old('chauffeure_id') == $chauffeure->id)
                                                    <option value="{{ $chauffeure->id }}" selected>{{ $chauffeure->nom }} {{ $chauffeure->prenom }}</option>
                                                @else
                                                    <option value="{{ $chauffeure->id }}">{{ $chauffeure->nom }} {{ $chauffeure->prenom }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span style="color: red">@error('chauffeure_id'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">Date :<span style="color: red"> *</span></label>
                                        <input id="date" type="date" value="{{ old('date') }}" name="date" class="form-control @if($errors->get('date')) border border-danger @endif" required="required" >
                                        <span style="color: red">@error('date'){{$message}}@enderror</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="Vignette">Vignette</label>
                                      <input id="Vignette" type="TEXT" value="{{ old('Vignette') }}" name="Vignette" class="form-control @if($errors->get('Vignette')) border border-danger @endif" placeholder="Vignette">
                                      <span style="color: red">@error('Vignette'){{$message}}@enderror</span>
                                  </div>
                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label for="montant">Montant</label>
                                    <div class="input-group mb-3">
                                        <input id="montant" type="Number" value="{{ old('montant') }}" name="montant" class="form-control @if($errors->get('montant')) border border-danger @endif" placeholder="montant ">
                                        <div class="input-group-append">
                                          <span class="input-group-text">DH</span>
                                        </div>
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <span style="color: red">@error('montant'){{$message}}@enderror</span>
                            </div>

                          </div>
                          
                          <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Observation</label>
                                        <textarea id="Observation" name="Observation" class="form-control @if($errors->get('Observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ old('Observation') }}</textarea>
                                        <span style="color: red">@error('Observation'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>


                            <div class="row">                             
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted">
                                        <strong><span style="color: red">* est obligatoire.</span></strong>
                                </div>
                            </div>
                        </div>
    
                    </form>
            </div>
        </div>
      </div>
</div>
@endsection
