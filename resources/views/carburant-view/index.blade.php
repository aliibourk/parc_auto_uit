@extends('layouts.app')

@section('title')
    index Carburant
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase">LISTE DES CARBURANTS</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/Carburant">Carburants</a> </li>
        </ol>
    </div>

@can('create carburant')
    <center>
        <a class="btn btn-primary" style="font-size: 20px" href="{{ route('Carburant.create') }} "> <i class="fas fa-plus-square"></i><span> Ajouter une Carburant</a>
    </center>
@endcan
<BR>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">#</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Vehicule</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Chauffeure</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Date</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Vignette</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Montant</span></td>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Observation</span></td>
                                 @can('edit carburant')
                                    <td style="border:none;white-space: nowrap"><span class="text-uppercase">Action</span></th>
                                @endcan 
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @forelse($Carburants as $Carburant)
                                <tr>
                                    <td>
                                        <a>{{ $Carburant->id }}</a>
                                    </td>
                                    <td>
                                        @if ($Carburant->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($Carburant->vehicule_id == $vehicule->id)

                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif

                                                    {{ $vehicule->type }} : {{ $vehicule->marque }}
                                                    {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td  style="white-space: nowrap">
                                        @if ($Carburant->chauffeure_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_chauffeure as $chauffeure)
                                                @if ($Carburant->chauffeure_id == $chauffeure->id)
                                                    {{ $chauffeure->prenom }} {{ $chauffeure->nom }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td  style="white-space: nowrap">
                                        @if ($Carburant->date == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Carburant->date }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Carburant->Vignette == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Carburant->Vignette }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Carburant->montant == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Carburant->montant }} DH
                                        @endif
                                    </td>
                                    <td>
                                        @if ($Carburant->Observation == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Carburant->Observation }}
                                        @endif
                                    </td>
                                    @can('edit carburant')

                                    <td>
                                        <center>
                                            <a href="{{ route('Carburant.edit', $Carburant->id), '/edit' }}"
                                                class="Edit" title="Modifier" data-toggle="tooltip"><i
                                                    class="fas fa-edit fa-lg"></i></i></a>
                                            {{-- <a href="#" class="delete" title="Supprimer" data-toggle="modal"
                                                data-target="#exampleModal-{{ $Carburant->id }}"
                                                data-whatever="{{ $Carburant->id }}"><i
                                                    class="fas fa-trash-alt fa-lg"></i></a> --}}
                                                    
                                            {{-- Model --}}
                                            {{-- <div class="modal fade" id="exampleModal-{{ $Carburant->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form method="POST"
                                                    action="{{ route('Carburant.destroy', $Carburant->id) }}"
                                                    role="form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">confirmation
                                                                </h5>
                                                                <input type="hidden" name="dele"
                                                                    value="{{ $Carburant->id }}">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3> voulez vous supprimer la Carburant Referance :
                                                                    {{ $Carburant->id }}</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-danger btn-send"
                                                                    value="Supprimer">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> --}}
                                        </center>
                                    </td>
                                  @endcan 

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Carburant</center>
                                            </strong> </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
