@extends('layouts.app')

@section('title')
    index Assaurance
@endsection

@section('content')


    <center>
        <h1 class="mt-4"><span class="text-uppercase">LISTE DES ASSAURANCES</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/Assaurance">Assaurances</a> </li>
        </ol>
    </div>

    @can('create assaurance')
        <center>
            <a class="btn btn-primary" style="font-size: 20px" href="{{ route('Assaurance.create') }} "> <i class="fas fa-plus-square"></i><span> Ajouter une Assaurance</a>
        </center>
    @endcan

    <BR>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">#</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Vehicule</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Assaurance</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Date debut</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Duree d'assaurance</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Status</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Date fin</span></th>
                                <td style="border:none;white-space: nowrap"><span class="text-uppercase">Montant</span></th>
                                @can('edit assaurance')
                                    <td style="border:none;white-space: nowrap"><span class="text-uppercase">Action</span></th>
                                @endcan                         
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @forelse($Assaurances as $Assaurance)
                                <tr>
                                    <td style="white-space: nowrap">
                                        <a>{{ $Assaurance->id }}</a>
                                    </td>
                                    <td style="white-space: nowrap">

                                        @if ($Assaurance->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($Assaurance->vehicule_id == $vehicule->id)

                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif

                                                    {{ $vehicule->type }} : {{ $vehicule->marque }}
                                                    {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif

                                    </td>
                                    <td style="white-space: nowrap">

                                        @if ($Assaurance->entreprise_assaurance_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_assaurance as $assaur)
                                                @if ($Assaurance->entreprise_assaurance_id == $assaur->id)
                                                    {{ mb_strimwidth($assaur->NomAssaurance, 0, 10, '...') }}
                                                @endif
                                            @endforeach
                                        @endif

                                    </td>
                                    <td style="white-space: nowrap">

                                        @if ($Assaurance->date_debut == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $Assaurance->date_debut }}
                                        @endif

                                    </td>
                                    <td style="white-space: nowrap">
                                        <center>
                                            @if ($Assaurance->duree == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Assaurance->duree }} Mois
                                            @endif
                                        </center>
                                    </td>
                                    <td style="white-space: nowrap">
                                        <center>
                                            @if (\Carbon\Carbon::parse($Assaurance->date_debut)->addMonths($Assaurance->duree) < now())
                                                <span class="badge badge-pill  badge-danger">Expiré</span>
                                            @else
                                                <span class="badge badge-pill  badge-success">Valide</span>
                                            @endif
                                        </center>
                                    </td>
                                    <td style="white-space: nowrap">
                                        <center>
                                            @if ($Assaurance->duree == null)
                                                <span style="color: red">---</span>
                                            @else
                                                @if (\Carbon\Carbon::parse($Assaurance->date_debut)->addMonths($Assaurance->duree) < now())
                                                    <span class="badge badge-pill  badge-danger">
                                                        {{ \Carbon\Carbon::parse($Assaurance->date_debut)->addMonths($Assaurance->duree)->diffForHumans() }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-pill  badge-success">
                                                        {{ \Carbon\Carbon::parse($Assaurance->date_debut)->addMonths($Assaurance->duree)->diffForHumans() }}
                                                    </span>
                                                @endif
                                            @endif
                                        </center>
                                    </td>
                                    <td style="white-space: nowrap">
                                        <center>
                                            @if ($Assaurance->montant == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Assaurance->montant }} DH
                                            @endif
                                        </center>
                                    </td>
                                    @can('edit assaurance')

                                    <td style="white-space: nowrap">
                                        <center>
                                            <a href="{{ route('Assaurance.edit', $Assaurance->id), '/edit' }}"
                                                class="Edit" title="Modifier" data-toggle="tooltip"><i
                                                    class="fas fa-edit fa-lg"></i></i></a>

                                            {{-- <a href="#" class="delete" title="Supprimer" data-toggle="modal"
                                                data-target="#exampleModal-{{ $Assaurance->id }}"
                                                data-whatever="{{ $Assaurance->id }}"><i
                                                    class="fas fa-trash-alt fa-lg"></i></a> --}}

                                            {{-- Model --}}
                                            {{-- <div class="modal fade" id="exampleModal-{{ $Assaurance->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form method="POST"
                                                    action="{{ route('Assaurance.destroy', $Assaurance->id) }}"
                                                    role="form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">confirmation
                                                                </h5>
                                                                <input type="hidden" name="dele"
                                                                    value="{{ $Assaurance->id }}">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>
                                                                    @foreach ($liste_vehicule as $vehicule)
                                                                        @if ($Assaurance->vehicule_id == $vehicule->id)
                                                                            voulez vous supprimer l'assaurance de
                                                                            {{ $vehicule->type }} : <br>
                                                                            {{ $vehicule->marque }}
                                                                            {{ $vehicule->immatriculation }}
                                                                        @endif
                                                                    @endforeach
                                                                </h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-danger btn-send"
                                                                    value="Supprimer">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> --}}
                                        </center>
                                    </td>
                                    @endcan 

                                </tr>

                            @empty
                                <tr>
                                    <td colspan="9">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Assaurance</center>
                                            </strong> </div>
                                    </td>
                                </tr>

                            @endforelse


                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
