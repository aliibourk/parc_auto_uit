@extends('layouts.app')

@section('title')
Ajouter Assaurance 
@endsection

@section('content')
<center>
    <h1 class="mt-4">AJOUTER ASSAURANCE</h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('Assaurance.index') }}">Assaurances &nbsp;</a><a href="/Assaurance/create">/ Ajouter</a>
        </li>
        {{-- <button class="btn btn-danger"><a href="/Vehicules_to_excel">Exporter Excel</a> </button>
    <button class="btn btn-info"><a href="/Vehicules_to_csv">Exporter CSV</a> </button> --}}
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouvelle Assaurance</h3>
            </div>
            <div class="card-body">

                <form id="add-form" method="post" action="{{ route('Assaurance.store') }}" role="form">
                    @csrf
                    <div class="messages"></div>
                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                    <select id="vehicule_id"  value="{{ old('vehicule_id') }}" name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                                        <option value="" selected disabled>selectionner une vehicule</option>
                                        @foreach ($liste_vehicule as $vehicule)
                                            @if (old('vehicule_id') == $vehicule->id)
                                            <option value="{{ $vehicule->id }}" selected>{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                            @else
                                            <option value="{{ $vehicule->id }}">{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="entreprise_assaurance_id">Entreprise d'assaurance :<span style="color: red"> *</span></label>
                                    <select id="entreprise_assaurance_id" name="entreprise_assaurance_id" class="form-control @if($errors->get('entreprise_assaurance_id')) border border-danger @endif" required="required" >
                                        
                                        <option value="" selected disabled>selectionner une assaurance</option>
                                        @foreach ($liste_assaurance as $assaur)
                                            @if (old('entreprise_assaurance_id') == $assaur->id)
                                                <option value="{{ $assaur->id }}" selected>{{ $assaur->NomAssaurance }}</option>
                                            @else
                                                <option value="{{ $assaur->id }}">{{ $assaur->NomAssaurance }}</option>
                                            @endif
                        
                                        @endforeach
                                    </select>
                                    <span style="color: red">@error('entreprise_assaurance_id'){{$message}}@enderror</span>
                                </div>
                            </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_debut">Date Debut :<span style="color: red"> *</span></label>
                                            <input id="date_debut" value="{{ old('date_debut') }}" type="date" name="date_debut" class="form-control @if($errors->get('date_debut')) border border-danger @endif" required="required" >
                                            <span style="color: red">@error('date_debut'){{$message}}@enderror</span>
                                        </div>
                                    </div>
    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="duree">Duree :<span style="color: red"> *</span></label>
                                            <div class="input-group mb-3">
                                                <input id="duree" type="number" value="{{ old('duree') }}" name="duree" class="form-control @if($errors->get('duree')) border border-danger @endif" placeholder="duree" required="required">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">Mois</span>
                                                </div>
                                                <span style="color: red">@error('duree'){{$message}}@enderror</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="montant">Montant :</label>
                                            <div class="input-group mb-3">
                                                <input id="montant" type="number" value="{{ old('montant') }}" name="montant" class="form-control @if($errors->get('montant')) border border-danger @endif" placeholder="montant ">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">DH</span>
                                                </div>    
                                                <span style="color: red">@error('montant'){{$message}}@enderror</span>
                                            </div>
                                        </div>
                                    </div>  
                                </div>

                                <div class="row">                             
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-muted">
                                            <strong><span style="color: red">* est obligatoire.</span></strong>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection


