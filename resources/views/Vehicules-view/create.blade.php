@extends('layouts.app')

@section('title')
Ajouter Vehicule 
@endsection
 
@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER Vehicule</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('vehicules.index') }}">Vehicules &nbsp;</a><a href="/vehicules/create">/ Ajouter</a>
        </li>
        {{-- <button class="btn btn-danger"><a href="/Vehicules_to_excel">Exporter Excel</a> </button>
    <button class="btn btn-info"><a href="/Vehicules_to_csv">Exporter CSV</a> </button> --}}
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau vehicule</h3>
            </div>
            <div class="card-body">

<form id="add-form" method="post" action="{{ route('vehicules.store') }}" role="form">
    <div class="messages"></div>
    @csrf
    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="immatriculation">N° Immatriculation <span style="color: red"> *</span></label>
                    <input id="immatriculation" value="{{ old('immatriculation') }}" type="text" name="immatriculation" class="form-control @if($errors->get('immatriculation')) border border-danger @endif" placeholder="immatriculation *" required="required" data-error="immatriculation est obligatoire.">
                    <span style="color: red">@error('immatriculation'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="type">Type <span style="color: red"> *</span></label>
                  <select id="type" name="type" value="{{ old('type') }}" class="form-control @if($errors->get('type')) border border-danger @endif" required="required" data-error="type est obligatoire.">
                    <option value="" selected disabled>selectionner le type de vehicule</option>
                        @if (old('type') != null)
                            <option value="{{ old('type') }}" selected>{{ old('type') }}</option>
                        @endif 
                    <option value="Voiture">Voiture</option>
                    <option value="Moto">Moto</option>
                    <option value="Velo">Velo</option>
                    <option value="Bus">Bus</option>
                    <option value="MiniBus">MiniBus</option>
                  </select>
                  <span style="color: red">@error('type'){{$message}}@enderror</span>
              </div>
          </div>
        </div>

        <div class="row">

          {{-- <div class="col-md-6">
            <div class="form-group">
                <label for="marque">Marque <span style="color: red"> *</span></label>
                <select id="marque" value="{{ old('marque') }}" name="marque" class="form-control @if($errors->get('marque')) border border-danger @endif" required="required" data-error="marque">
                    <option value="" selected disabled>selectionner la marque de vehicule</option>
                        @if (old('marque') != null)
                            <option value="{{ old('marque') }}" selected>{{ old('marque') }}</option>
                        @endif                    
                    <option value="Mercedes Sprinter">Mercedes Sprinter</option>
                    <option value="Dacia Lodgy">Dacia Lodgy</option>
                    <option value="AutoCar Volvo">AutoCar Volvo</option>
                    <option value=">MitsibushiP15 Whizl L300">MitsibushiP15 Whizl L300</option>
                    <option value="Volkswagen Passat">Volkswagen Passat</option>
                    <option value="Partner">Partner</option>
                    <option value="Man AUTO CAR">Man AUTO CAR</option>
                    <option value="Kango">Kango</option>
                    <option value="toyota">toyota</option>
                    <option value="Dacia logan">Dacia logan</option>
                    <option value="Peugo Ion">Peugo Ion</option>
                    <option value="Vélomoteur">Vélomoteur </option>
                    <option value="Velo Eléctrique">Vélo Eléctrique</option>
                </select>
                <span style="color: red">@error('marque'){{$message}}@enderror</span>
            </div>
        </div> --}}

        <div class="col-md-6">
            <div class="form-group">
                <label for="marque">Marque <span style="color: red"> *</span></label>
                <input id="marque" value="{{ old('marque') }}" type="text" name="marque" class="form-control @if($errors->get('marque')) border border-danger @endif" placeholder="Marque *" required="required" data-error="Marque est obligatoire.">
                <span style="color: red">@error('marque'){{$message}}@enderror</span>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="form-group">
                <label for="Carburan">Carburan <span style="color: red"> *</span></label>
                <select id="Carburan" value="{{ old('Carburan') }}" name="Carburan" class="form-control @if($errors->get('Carburan')) border border-danger @endif" required="required" data-error="Carburan">
                    <option value="" selected disabled>selectionner le Carburan de vehicule</option>
                        @if (old('Carburan') != null)
                            <option value="{{ old('Carburan') }}" selected>{{ old('Carburan') }}</option>
                        @endif
                    <option value="diesel" >diesel</option>
                    <option value="Essence">Essence</option>
                    <option value="electrique">electrique</option>
                    <option value="none">none</option>
                </select>
                <span style="color: red">@error('Carburan'){{$message}}@enderror</span>
            </div>
        </div>
      </div>


        <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <label for="puissance">Puissance <span style="color: red"> *</label>
                  <input id="puissance" value="{{ old('puissance') }}" required type="number" name="puissance" class="form-control @if($errors->get('puissance')) border border-danger @endif" placeholder="puissance">
                  <span style="color: red">@error('puissance'){{$message}}@enderror</span>
                </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                  <label for="Usage">Usage</label>
                  <input id="Usage" value="{{ old('Usage') }}" type="text" name="Usage" class="form-control @if($errors->get('Usage')) border border-danger @endif" placeholder="Usage ">
                  <span style="color: red">@error('Usage'){{$message}}@enderror</span>
              </div>
          </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="N_Chassis">N Chassis <span style="color: red"> *</label>
                <input id="N_Chassis" value="{{ old('N_Chassis') }}" required type="text" name="N_Chassis" class="form-control @if($errors->get('N_Chassis')) border border-danger @endif" placeholder="Numero Chassis">
                <span style="color: red">@error('N_Chassis'){{$message}}@enderror</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="date_mise_en_route">Date Mise En Route <span style="color: red"> *</label>
                <input id="date_mise_en_route"  value="{{ old('date_mise_en_route') }}" required  type="date" name="date_mise_en_route" class="form-control @if($errors->get('date_mise_en_route')) border border-danger @endif" placeholder="date mise en route ">
                <span style="color: red">@error('date_mise_en_route'){{$message}}@enderror</span>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
            <label for="etablissement">Etablissement <span style="color: red"> *</span></label>
            <select id="etablissement" value="{{ old('etablissement') }}" name="etablissement" class="form-control @if($errors->get('etablissement')) border border-danger @endif" required="required">
                <option value="" selected disabled>selectionner une etablissement</option>
                @if (old('etablissement') != null)
                    <option value="{{ old('etablissement') }}" selected>{{ old('etablissement') }}</option>
                @endif
              <option value="Uit" >Uit</option>
              <option value="ENCG">ENCG</option>
              <option value="EST">EST</option>
              <option value="ENSA">ENSA</option>
              <option value="Présidence">Présidence</option>
              <option value="Faculté des Sciences">Faculté des Sciences</option>
              <option value="institut des metiere de sport">institut des metiere de sport</option>
              <option value="Faculté des langues, des lettres et des arts">Faculté des langues, des lettres et des arts</option>
              <option value="Faculté des sciences humaines et sociales">Faculté des sciences humaines et sociales</option>
              <option value="Faculté d’économie et de gestion">Faculté d’économie et de gestion</option>
              <option value="Faculté des sciences juridiques et politiques">Faculté des sciences juridiques et politiques</option>
              <option value="Ecole nationale superieure de chimie">Ecole nationale superieure de chimie</option>
              <option value="ECOLE superieure d'education et de formation">ECOLE superieure d'education et de formation</option>
            </select>
            <span style="color: red">@error('etablissement'){{$message}}@enderror</span>
        </div>
        </div>
        <div class="col-md-6">
              <div class="form-group">
                 <label for="Nbre_de_place">Number De Place <span style="color: red"> *</label>
                 <input id="Nbre_de_place" value="{{ old('Nbre_de_place') }}" required type="number" name="Nbre_de_place" class="form-control @if($errors->get('Nbre_de_place')) border border-danger @endif" placeholder="number de place">
                 <span style="color: red">@error('Nbre_de_place'){{$message}}@enderror</span>
                </div>
        </div>
     </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="form_message">Observation</label>
                    <textarea id="Observation" name="Observation" class="form-control @if($errors->get('Observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ old('Observation') }}</textarea>
                    <span style="color: red">@error('Observation'){{$message}}@enderror</span>
                </div>
            </div>
                                
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    <strong><span style="color: red">* est obligatoire.</span></strong>
            </div>
        </div>
    </div>
    
</form>


</div>
</div>
</div>
</div>
@endsection
