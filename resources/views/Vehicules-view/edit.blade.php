@extends('layouts.app')

@section('title')
Edit vehicule 
@endsection

@section('content')
{{-- @can('update') --}}
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">MODIFIER Vehicule</span> </h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="/Vehicules">Vehicules &nbsp;</a><a href="{{ route('vehicules.edit', $Vehicule->id), '/edit' }}">/ Modifier</a>
        </li>
    </ol>
</div>


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Modifier {{ $Vehicule->type}} : {{ $Vehicule->marque}} {{ $Vehicule->immatriculation }}</h3>
            </div>
            <div class="card-body">
                

    <form id="add-form" method="POST" action="{{ route('vehicules.update' , ['vehicule' => $Vehicule->id]) }}" role="form">
      @csrf
      @method('PUT')
      <div class="messages"></div>
      <div class="controls">

          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="immatriculation">N° Immatriculation <span style="color: red"> *</span></strong></label>
                      <input id="immatriculation" value="{{ $Vehicule->immatriculation }}" disabled type="text" class="form-control" placeholder="immatriculation" required="required" data-error="immatriculation est obligatoire.">
                  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="type">Type <span style="color: red"> *</span></strong></label>
                    <input id="type" value="{{ $Vehicule->type }}" disabled type="text" class="form-control" placeholder="type" required="required" data-error="type est obligatoire.">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="marque">Marque <span style="color: red">*</span></strong></label>
                  <input id="marque" value="{{ $Vehicule->marque }}" disabled type="text" class="form-control" placeholder="marque" required="required" data-error="marque est obligatoire.">
              </div>
          </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="Carburan">Carburan <span style="color: red">*</span></strong></label>
                  <input id="Carburan" value="{{ $Vehicule->Carburan }}" disabled type="text" class="form-control" placeholder="Carburan" required="required">
                </div>
            </div>
        </div>
          <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="puissance">Puissance</label>
                    <input id="puissance" type="number" value="{{ $Vehicule->puissance }}" disabled class="form-control" placeholder="puissance">
                    <span style="color: red">@error('puissance'){{$message}}@enderror</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Usage">Usage</label>
                    <input id="Usage" type="text" value="{{ $Vehicule->Usage }}" name="Usage" class="form-control @if($errors->get('Usage')) border border-danger @endif" placeholder="Usage ">
                    <span style="color: red">@error('Usage'){{$message}}@enderror</span>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <label for="N_Chassis">N Chassis</label>
                  <input id="N_Chassis" type="text" disabled value="{{ $Vehicule->N_Chassis }}" class="form-control" placeholder="Numero Chassis">
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                  <label for="date_mise_en_route">Date Mise En Route</label>
                  <input id="date_mise_en_route" disabled type="date" value="{{ $Vehicule->date_mise_en_route }}" class="form-control">
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
              <label for="etablissement">Etablissement <span style="color: red">*</span></strong></label>
              <select id="etablissement" name="etablissement" value="{{ $Vehicule->etablissement }}" class="form-control @if($errors->get('etablissement')) border border-danger @endif">
                <option value="{{ $Vehicule->etablissement }}" selected>{{ $Vehicule->etablissement }}</option>
                <option value="Uit" >Uit</option>
                <option value="ENCG">ENCG</option>
                <option value="EST">EST</option>
                <option value="ENSA">ENSA</option>
                <option value="Presidence">Présidence</option>
                <option value="institut des metiere de sport">institut des metiere de sport</option>
                <option value="Faculté des langues, des lettres et des arts">Faculté des langues, des lettres et des arts</option>
                <option value="Faculté des sciences humaines et sociales">Faculté des sciences humaines et sociales</option>
                <option value="facculte des science">facculte des science </option>
                <option value="Faculté d’économie et de gestion">Faculté d’économie et de gestion</option>
                <option value="Faculté des sciences juridiques et politiques">Faculté des sciences juridiques et politiques</option>
                <option value="Ecole nationale superieure de chimie">Ecole nationale superieure de chimie</option>
                <option value="ECOLE superieure d'education et de formation">ECOLE superieure d'education et de formation</option>
              </select>
              <span style="color: red">@error('etablissement'){{$message}}@enderror</span>
          </div>
      </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="Nbre_de_place">Number De Place</label>
                <input id="Nbre_de_place" type="number" disabled value="{{ $Vehicule->Nbre_de_place }}" class="form-control">
            </div>
        </div>
    </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="form_message">Observation</label>
                      <textarea id="Observation" name="Observation" class="form-control @if($errors->get('Observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ $Vehicule->Observation }}</textarea>
                      <span style="color: red">@error('Observation'){{$message}}@enderror</span>
                  </div>
              </div> 
               
              <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-send" value="Modifier">
            </div>  
              
          </div>
          <div class="row">
              <div class="col-md-12">
                  <p class="text-muted">
                      <strong><span style="color: red">* est obligatoire.</span></strong>
              </div>
          </div>
      </div>

  </form>
</div>
        </div>
      </div>
</div>
{{-- @endcan --}}

@endsection
