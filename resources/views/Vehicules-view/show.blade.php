@extends('layouts.app')

@section('title')
show
@endsection


@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">

<div class="card-header"> <h4><strong>{{ $Vehicule->type}} : {{ $Vehicule->marque}} / N° Immatriculation : {{ $Vehicule->immatriculation }}</strong></h4> </div>    
    <div class="card-header">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-sm">
                <tr>
                    <td>
                        <center>
                            <a class="btn btn-primary" href="{{ route('vehicules.edit' , ['vehicule' => $Vehicule->id]),'/edit' }}">Editer</a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <form method="POST" action="{{ route('vehicules.destroy' , ['vehicule' => $Vehicule->id]) }}" role="form">
                                @csrf
                                @method('DELETE')
                                {{-- <input type="submit" class="btn btn-danger btn-send" value="Supprimer"> --}}

                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                    Supprimer
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">confirmation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            <h3> voulez vous supprimer cette Vehicule : {{ $Vehicule->type}} : {{ $Vehicule->marque}} / N° Immatriculation : {{ $Vehicule->immatriculation }}</h3>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-danger btn-send" value="Supprimer">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </center>    
                    </td>
                </tr>
            </table>
        </div>    
    </div>    
   


<div class="card-body">
    
<table id="dtDynamicVerticalScrollExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <tbody>

    
        <tr>
            <th>Type</th>
            <td>@if ($Vehicule->type == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->type}} 
                @endif
            </td>
        </tr>
        <tr>
            <th>Marque</th>
            <td>@if ($Vehicule->marque == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->marque}}
                @endif
            </td>
        </tr>
        <tr>
            <th>Puissance</th>
            <td>@if ($Vehicule->puissance == null)
                    <em style="color: red">null</em> 
                @else
                    @if ( $Vehicule->Carburan == 'electrique')
                        {{$Vehicule->puissance}} <span>W</span> 
                    @else
                        {{$Vehicule->puissance}} <span>cv</span>
                    @endif  
                @endif
            </td>
        </tr>
        <tr>
            <th>Date mise en route</th>
            <td>@if ($Vehicule->date_mise_en_route == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->date_mise_en_route}}
                @endif
            </td>
        </tr>
        <tr>
            <th>N Chassis</th>
            <td>@if ($Vehicule->N_Chassis == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->N_Chassis}}
                @endif
            </td>
        </tr>
        <tr>
            <th>Carburan</th>
            <td>@if ($Vehicule->Carburan == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->Carburan}}
                @endif
            </td>
        </tr>
        <tr>
            <th>Etablissement</th>
            <td>@if ($Vehicule->etablissement == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->etablissement}}
                @endif
            </td>
        </tr>
        <tr>
            <th>nombre de place</th>
            <td>@if ($Vehicule->Nbre_de_place == null)
                    <em style="color: red">null</em> 
                @else
                    {{$Vehicule->Nbre_de_place}}
                @endif
            </td>
        </tr>
        <tr>
            <th>Usage</th>
            <td>@if ($Vehicule->Usage == "")
                <em style="color: red">null</em> 
            @else
                {{$Vehicule->Usage}}
            @endif
            </td>
        </tr>
        <tr>
            <th>Observation</th>
            <td>@if ($Vehicule->Observation == "") 
                    <textarea name="" id="" cols="100" rows="5" disabled>NULL</textarea> 
                @else
                    <textarea name="" id="" cols="100" rows="5" disabled>{{$Vehicule->Observation}}</textarea> 
                @endif
            </td>  
        </tr>

    </tbody>

    

</table>
</div> 
</div>
</div>
</div>
</div>

@endsection
