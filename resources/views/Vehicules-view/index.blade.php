@extends('layouts.app')

@section('title')
    index Vehicules 
@endsection

@section('content')

    <center>
        <h1 class="mt-4">
            <span class="text-uppercase">
                @if ($archive == true)
                    <span style="color: RGB(220, 53, 69)">{{ $TypeList }}</span>
                @else
                    <span>{{ $TypeList }}</span>
                @endif
            </span>
        </h1>
        <div>
            <h5>{{ $Vehicules->count() }} Vehicule(s)</h5>
        </div>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">
                <a href="/vehicules">Vehicules &nbsp;</a> 
                @if ($archive == true)
                    <a href="/vehicules/archive">/ Archive</a>
                @endif
            </li>
        </ol>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">N° Imm</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Type</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Marque</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Puissance</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Mise en route</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">N Chassis</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Carburan</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Etablissement</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">N de place</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Usage</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Observation</td>

                                @if ($archive == true)
                                    @canany(['forcedelete vehicule', 'restore vehicule'])
                                        <td  class="text-right text-uppercase" style="border:none;white-space: nowrap">ACTION</td>
                                    @endcan
                                @else
                                    @canany(['edit vehicule', 'delete vehicule'])
                                        <td  class="text-right text-uppercase" style="border:none;white-space: nowrap">ACTION</td>
                                    @endcan
                                @endif
                                
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @forelse($Vehicules as $vehicule)
                                <tr>
                                    <td style="white-space: nowrap">
                                        <a>{{ $vehicule->immatriculation }}</a>    
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->type == null)
                                            <span style="color: red">---</span>
                                        @else

                                            @if ($vehicule->type == 'Voiture')
                                                <i class="fas fa-car"></i>
                                            @else
                                                @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                    <i class="fas fa-bicycle"></i>
                                                @else
                                                    @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                        <i class="fas fa-bus-alt"></i>
                                                    @endif
                                                @endif
                                            @endif
                                            {{ $vehicule->type }}
                                        @endif
                                    </td>
                                    <td >
                                        @if ($vehicule->marque == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->marque }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->puissance == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @if ($vehicule->Carburan == 'electrique')
                                                {{ $vehicule->puissance }} <span>W</span>
                                            @else
                                                {{ $vehicule->puissance }} <span>cv</span>
                                            @endif
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->date_mise_en_route == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->date_mise_en_route }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->N_Chassis == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->N_Chassis }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->Carburan == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->Carburan }}
                                        @endif
                                    </td>
                                    <td >
                                        @if ($vehicule->etablissement == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{-- {{substr($vehicule->etablissement, 0, 20)}} --}}
                                            {{ mb_strimwidth($vehicule->etablissement, 0, 20, '...') }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->Nbre_de_place == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->Nbre_de_place }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->Usage == '')
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->Usage }}
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($vehicule->Observation == '')
                                            <span style="color: red">---</span>
                                        @else
                                            {{ $vehicule->Observation }}
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <center>
                                                
                                            @if ($archive == true)
                                                {{-- <a href="{{ url('vehicules.Restore', $vehicule->id), '/edit' }}" class="Restore" title="Restore" data-toggle="tooltip"><i class="fas fa-trash-restore"></i></a> --}}
                                                {{-- <a href="{{ url('/vehicules/', $vehicule->id, '/restore') }}" class="Restore" 
                                                        title="Restore">
                                                        <i class="fas fa-trash-restore"></i>
                                                    </a> --}}
                                                   
                                                @can('restore vehicule')
                                                    <form method="POST" class="fm-inline" action="{{ url('vehicules/'.$vehicule->id.'/restore') }}">
                                                    @csrf
                                                    @method('PATCH')

                                                    <input type="submit" value="Restaurer" class="btn btn-outline-info btn-sm">
                                                </form>
                                                @endcan
                                                
                                                @can('forcedelete vehicule')
                                                        
                                                <input type="submit" value="Supprimer" class="btn btn-outline-danger btn-sm" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $vehicule->id }}" data-whatever="{{ $vehicule->id }}">

                                                {{-- Model --}}
                                                <div class="modal fade" id="exampleModal-{{ $vehicule->id }}" tabindex="-1"
                                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <form method="POST" action="{{ url('vehicules/'.$vehicule->id.'/forcedelete') }}"
                                                        role="form">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                        confirmation</h5>
                                                                    <input type="hidden" name="dele"
                                                                        value="{{ $vehicule->id }}">
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h3> voulez vous supprimer définitivement la vehicule Referance :
                                                                        {{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                    <input type="submit" class="btn btn-danger btn-send"
                                                                        value="Supprimer">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div> 
                                                @endcan

                                                
                                                
                                                
                                            @else
                                            @can('edit vehicule')
                                                <a href="{{ route('vehicules.edit', $vehicule->id), '/edit' }}" class="Edit" title="Modifier" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                                            @endcan

                                            @can('delete vehicule')
                                                <a href="#" class="delete" title="Supprimer" data-toggle="modal" data-target="#exampleModal-{{ $vehicule->id }}" data-whatever="{{ $vehicule->id }}"><i class="fas fa-trash-alt"></i></a>

                                                {{-- Model --}}
                                                <div class="modal fade" id="exampleModal-{{ $vehicule->id }}" tabindex="-1"
                                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <form method="POST" action="{{ route('vehicules.destroy', $vehicule->id) }}"
                                                        role="form">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                        confirmation</h5>
                                                                    <input type="hidden" name="dele"
                                                                        value="{{ $vehicule->id }}">
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h3> voulez vous supprimer la vehicule {{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                    <input type="submit" class="btn btn-danger btn-send"
                                                                        value="Supprimer">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div> 
                                                @endcan    
                                            @endif
                                                                                       
                                        </center>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Vehicule</center>
                                            </strong>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        @if ($archive == false)
            <div class="badge badge-success text-wrap" style="width: 16rem;">
                <a href="/vehicules/archive" class="text-light"><i class="fas fa-archive"></i> <small>Voir l'archive des vehicules --></small></a>
            </div>
        @else
            <div class="badge badge-danger text-wrap" style="width: 16rem;">
                <a href="/vehicules" class="text-light"><i class="fas fa-car"></i> <small>Voir les vehicules actual--></small></a>
            </div>
        @endif
    @endsection
