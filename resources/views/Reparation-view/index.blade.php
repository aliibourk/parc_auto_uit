@extends('layouts.app')

@section('title')
Reparations
@endsection

@section('content')

    <center>
        <h1 class="mt-4"><span class="text-uppercase">LISTE DES Reparations</span></h1>
    </center>
    <div class="container">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="/Reparation">Reparations</a> </li>
        </ol>
    </div>

@can('create reparation')
    <center>
        <a class="btn btn-primary" style="font-size: 20px" href="{{ route('Reparation.create') }} "> <i class="fas fa-plus-square"></i><span> Ajouter une Reparation</a>
    </center>
@endcan 
<BR>
                    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-primary">
                            <tr>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">#</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Vehicule</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Chauffeure</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Date</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Carburant</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">N Souche</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Object</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Kelometrage</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Benificiere</td>
                                <td style="border:none;white-space: nowrap" class="text-uppercase">Observation</td>
                                @can('edit reparation')
                                    <td style="border:none;white-space: nowrap" class="text-uppercase">Action</td>
                                @endcan 
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @forelse($Reparations as $Reparation)
                                <tr>
                                    <td style="white-space: nowrap">                                    
                                            <a>{{ $Reparation->id }}</a>                                       
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($Reparation->vehicule_id == null)
                                            <span style="color: red">---</span>
                                        @else
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if ($Reparation->vehicule_id == $vehicule->id)

                                                    @if ($vehicule->type == 'Voiture')
                                                        <i class="fas fa-car"></i>
                                                    @else
                                                        @if ($vehicule->type == 'Moto' || $vehicule->type == 'Velo')
                                                            <i class="fas fa-bicycle"></i>
                                                        @else
                                                            @if ($vehicule->type == 'Bus' || $vehicule->type == 'MiniBus')
                                                                <i class="fas fa-bus-alt"></i>
                                                            @endif
                                                        @endif
                                                    @endif

                                                    {{ $vehicule->marque }} : {{ $vehicule->immatriculation }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                       
                                            @if ($Reparation->chauffeure_id == null)
                                                <span style="color: red">---</span>
                                            @else
                                                @foreach ($liste_chauffeure as $chauffeure)
                                                    @if ($Reparation->chauffeure_id == $chauffeure->id)
                                                        {{ $chauffeure->prenom }} {{ $chauffeure->nom }}
                                                    @endif
                                                @endforeach
                                            @endif                                        
                                    </td>
                                    <td style="white-space: nowrap">                                       
                                            @if ($Reparation->date == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Reparation->date }}
                                            @endif                                       
                                    </td>
                                    <td style="white-space: nowrap">                                       
                                            @if ($Reparation->montant_vignette == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Reparation->montant_vignette }} DH
                                            @endif                                       
                                    </td>
                                    <td style="white-space: nowrap">                                       
                                            @if ($Reparation->N_Souche == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Reparation->N_Souche }}
                                            @endif                                      
                                    </td>
                                    <td style="white-space: nowrap">                                       
                                            @if ($Reparation->object == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Reparation->object }}
                                            @endif                                       
                                    </td>
                                    <td style="white-space: nowrap">                                        
                                            @if ($Reparation->kelometrage == null)
                                                <span style="color: red">---</span>
                                            @else
                                                {{ $Reparation->kelometrage }} kilo
                                            @endif                                        
                                    </td>
                                    <td style="white-space: nowrap">                                        
                                            @if ($Reparation->Garage_id == null)
                                                <span style="color: red">---</span>
                                            @else
                                                @foreach ($liste_garage as $garage)
                                                    @if ($garage->id == $Reparation->Garage_id)
                                                        {{ $garage->NomGarage }}
                                                    @endif
                                                @endforeach
                                            @endif
                                    </td>
                                    <td style="white-space: nowrap">
                                        @if ($Reparation->observation == null)
                                            <span style="color: red">---</span>
                                        @else
                                            {{ mb_strimwidth("$Reparation->observation", 0, 10, '...') }}
                                            {{-- {{$Reparation->observation}} --}}
                                        @endif
                                    </td>
                                @can('edit reparation')
                                    <td style="white-space: nowrap">
                                        <center>
                                            <a href="{{ route('Reparation.edit', $Reparation->id), '/edit' }}"
                                                class="Edit" title="edit" data-toggle="tooltip"><i
                                                    class="fas fa-edit fa-lg"></i></i></a>
                                            {{-- <a href="" class="delete" title="Supprimer" data-toggle="modal"
                                                data-target="#exampleModal-{{ $Reparation->id }}"
                                                data-whatever="{{ $Reparation->id }}"><i
                                                    class="fas fa-trash-alt fa-lg"></i></a> --}}

                                            {{-- Model --}}
                                            {{-- <div class="modal fade" id="exampleModal-{{ $Reparation->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form method="POST"
                                                    action="{{ route('Reparation.destroy', $Reparation->id) }}"
                                                    role="form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">confirmation
                                                                </h5>
                                                                <input type="hidden" name="dele"
                                                                    value="{{ $Reparation->id }}">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3> voulez vous supprimer la Reparation <br>
                                                                    Referance : {{ $Reparation->id }}</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-danger btn-send"
                                                                    value="Supprimer">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> --}}
                                        </center>
                                    </td>
                                @endcan 
                            </tr>

                            @empty
                                <tr>
                                    <td colspan="11">
                                        <div class="card-header"><strong style="color: red">
                                                <center>Aucune Reparation</center>
                                            </strong> </div>
                                    </td>
                                </tr>

                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
