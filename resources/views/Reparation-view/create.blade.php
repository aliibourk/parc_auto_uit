@extends('layouts.app')
 
@section('title')
Ajouter Reparation
@endsection

@section('content')
  
<center>
    <h1 class="mt-4"><span class="text-uppercase">AJOUTER Reparation</span></h1>
</center>
<div class="container">
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><a href="{{ route('Reparation.index') }}">Reparations &nbsp;</a><a href="/Reparation/create">/ Ajouter</a>
        </li>
    </ol>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>Ajouter Nouveau Reparation</h3>
            </div>
            <div class="card-body">
                
                      <form id="add-form" method="post" action="{{ route('Reparation.store') }}" role="form">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vehicule_id">Vehicule :<span style="color: red"> *</span></label>
                                        <select id="vehicule_id" name="vehicule_id" class="form-control @if($errors->get('vehicule_id')) border border-danger @endif" required="required">
                                            <option value="" selected disabled>selectionner une vehicule</option>
                                            @foreach ($liste_vehicule as $vehicule)
                                                @if (old('vehicule_id') == $vehicule->id)
                                                    <option value="{{ $vehicule->id }}" selected>{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                                @else
                                                    <option value="{{ $vehicule->id }}">{{ $vehicule->type}} : {{ $vehicule->marque}} {{ $vehicule->immatriculation }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span style="color: red">@error('vehicule_id'){{$message}}@enderror</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chauffeure_id">Chauffeure :<span style="color: red"> *</span></label>
                                        <select id="chauffeure_id" name="chauffeure_id" class="form-control @if($errors->get('chauffeure_id')) border border-danger @endif" required="required" >
                                            
                                            <option value="" selected disabled>selectionner un chauffeure</option>
                                            @foreach ($liste_chauffeure as $chauffeure)
                                                @if (old('chauffeure_id') == $chauffeure->id)
                                                    <option value="{{ $chauffeure->id }}" selected>{{ $chauffeure->nom }} {{ $chauffeure->prenom }}</option>
                                                @else
                                                    <option value="{{ $chauffeure->id }}">{{ $chauffeure->nom }} {{ $chauffeure->prenom }}</option>
                                                @endif    
                                            @endforeach
                                        </select>
                                        <span style="color: red">@error('chauffeure_id'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">Date :<span style="color: red"> *</span></label>
                                        <input id="date" type="date" value="{{ old('date') }}" name="date" class="form-control @if($errors->get('date')) border border-danger @endif" required="required" >
                                        <span style="color: red">@error('date'){{$message}}@enderror</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="montant_vignette">Montant Carburant :</label>
                                        <div class="input-group mb-3">
                                            <input id="montant_vignette" value="{{ old('montant_vignette') }}" type="number" name="montant_vignette" class="form-control @if($errors->get('montant_vignette')) border border-danger @endif" placeholder="montant carburant ">
                                            <div class="input-group-append">
                                              <span class="input-group-text">DH</span>
                                            </div>
                                            <span style="color: red">@error('montant_vignette'){{$message}}@enderror</span>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="object">Object :</label>
                                      <div class="input-group mb-3">
                                          <input id="object" value="{{ old('object') }}" type="text" name="object" class="form-control @if($errors->get('object')) border border-danger @endif" placeholder="object ">
                                          <span style="color: red">@error('object'){{$message}}@enderror</span>
                                        </div>
                                  </div>
                              </div>
  
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="Garage_id">Garage :<span style="color: red"> *</span></label>
                                      <select id="Garage_id" name="Garage_id" class="form-control @if($errors->get('Garage_id')) border border-danger @endif" required="required" >
                                          
                                          <option value="" selected disabled>selectionner un garage</option>
                                          @foreach ($liste_garage as $garage)
                                            @if (old('Garage_id') == $garage->id)
                                                <option value="{{ $garage->id }}" selected>{{ $garage->NomGarage }}</option>
                                            @else
                                                <option value="{{ $garage->id }}">{{ $garage->NomGarage }}</option>
                                            @endif 
                                          @endforeach
                                      </select>
                                      <span style="color: red">@error('Garage_id'){{$message}}@enderror</span>
                                  </div>
                              </div>
  
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kelometrage">Kelometrage :</label>
                                        <div class="input-group mb-3">
                                            <input id="kelometrage" value="{{ old('kelometrage') }}" type="NUMBER" name="kelometrage" class="form-control @if($errors->get('kelometrage')) border border-danger @endif" placeholder="kelometrage">
                                            <div class="input-group-append">
                                                <span class="input-group-text">kilomètre</span>
                                              </div>
                                                <span style="color: red">@error('kelometrage'){{$message}}@enderror</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="N_Souche">N Souche :</label>
                                        <div class="input-group mb-3">
                                            <input id="N_Souche" value="{{ old('N_Souche') }}" type="text" name="N_Souche" class="form-control @if($errors->get('N_Souche')) border border-danger @endif" placeholder="Numero de Souche">
                                        </div>
                                        <span style="color: red">@error('N_Souche'){{$message}}@enderror</span>  

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="observation">Observation :</label>
                                        <textarea id="observation" name="observation" class="form-control @if($errors->get('observation')) border border-danger @endif" placeholder="Observation" rows="4" >{{ old('observation') }}</textarea>
                                        <span style="color: red">@error('observation'){{$message}}@enderror</span>
                                    </div>
                                </div>
                            </div>
  


                            </div>
                          
                            <div class="row">                             
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary btn-send" value="Ajouter">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted">
                                        <strong><span style="color: red">* est obligatoire.</span></strong>
                                </div>
                            </div>
                        </div>
    
                    </form>
            </div>
        </div>
      </div>
@endsection
