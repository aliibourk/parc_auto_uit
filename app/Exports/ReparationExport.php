<?php

namespace App\Exports;

use App\Models\garage;
use App\Models\Reparation;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReparationExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'CHAUFFEURE',
            'GARAGE',
            'N SOUCHE',
            'DATE',
            'OBJECT',
            'KELOMETRAGE',
            'MONTANT',
            'OBSERVATION',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Reparation::all();

    }

    public function map($Reparation): array
    {
        $Garage = garage::where('id', $Reparation->Garage_id)->first();

        if (!$Reparation->montant_vignette) {
            $dh = "0";
        }else{
            $dh = $Reparation->montant_vignette;
        }

        if (!$Reparation->kelometrage) {
            $kelo = "0";
        }else{
            $kelo = $Reparation->kelometrage;
        }

        return [
            $Reparation->id,
            $Reparation->vehicule->immatriculation . " " . $Reparation->vehicule->marque,
            $Reparation->chauffeure->nom . " " . $Reparation->chauffeure->prenom,
            $Garage->NomGarage,
            $Reparation->N_Souche,
            $Reparation->date,
            $Reparation->object,
            $kelo . ' Kelo',
            $dh . ' DH',
            $Reparation->observation,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H','I','J'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:J1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Reparations'; 
    }

}
