<?php

namespace App\Exports;

use App\Models\garage;
use App\Models\VisiteTechnique;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VisiteTechniqueExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'CHAUFFEURE',
            'GARAGE',
            'DATE',
            'LA VISITE PROCHAINE',
            'N SOUCHE',
            'OBJECT',
            'KELOMETRAGE',
            'MONTANT',
            'OBSERVATION',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return VisiteTechnique::with('vehicule','garage','chauffeure')->get();
    }

    public function map($Visite): array
    {
        $Garage = garage::where('id', $Visite->Garage_id)->first();

        if (!$Visite->montant_vignette) {
            $dh = "0";
        }else{
            $dh = $Visite->montant_vignette;
        }

        return [
            $Visite->id,
            $Visite->vehicule->immatriculation . " " . $Visite->vehicule->marque,
            $Visite->chauffeure->nom . " " . $Visite->chauffeure->prenom,
            $Garage->NomGarage,
            $Visite->date,
            $Visite->next_Visite  . ' Mois',
            $Visite->N_Souche,
            $Visite->object,
            $Visite->kelometrage . ' K',
            $dh . ' DH',
            $Visite->Observation,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H','I','J','K'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:K1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Visites Technique'; 
    }
}
