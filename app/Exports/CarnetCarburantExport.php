<?php

namespace App\Exports;

use App\Models\vehicule;
use App\Models\CarnetCarburant;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CarnetCarburantExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'DATE CREATION',
            'DEBUT',
            'FIN',
            'N PAGE TOTAL DE CARNET',
            'N PAGE CONSOMMÉ',
            'STATUS',
        ];
    }

/**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return CarnetCarburant::with('vehicule')->get();

    // }



    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $Carnets_clean = CarnetCarburant::withTrashed()->whereIn('vehicule_id', vehicule::all()->pluck('id')->toArray())->get();

        // $v = vehicule::where('id', $CaCa->vehicule_id)->first();





        // return CarnetCarburant::all();
        return $Carnets_clean;
        // ::with('vehicule')->get();
        // dd(CarnetCarburant::withTrashed()->get());

        // return CarnetCarburant::all();

    }

    public function map($CaCa): array
    {
        if (!$CaCa->Npage_consomed) {
            $np = "0";
        }else{
            $np = $CaCa->Npage_consomed;
        }

        if ($CaCa->Npage_total > $CaCa->Npage_consomed) {
            $status = "Valide";
        }else{
            $status = "Expiré";
        }
        
        //------------------------------------------------------


        // $v = vehicule::where('id', $CaCa->vehicule_id)->first();
        // $immat = $v->immatriculation;
        // dd($immat);
        // $marque = $v->marque;
        // dd($marque. " " .$immat);

        // $MV = $CaCa->vehicule->immatriculation . " " . $CaCa->vehicule->marque;
        // dd($MV);

        
        //------------------------------------------------------


        return [
            $CaCa->id,
            // dd($CaCa->vehicule->immatriculation. " " . $CaCa->vehicule->marque),
            $CaCa->vehicule->immatriculation. " " . $CaCa->vehicule->marque,
            $CaCa->Date_creation,
            $CaCa->debut,
            $CaCa->fin,
            $CaCa->Npage_total,
            $np,
            $status,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Carnets Carburant'; 
    }

}
