<?php

namespace App\Exports;

use App\Models\Carburant;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CarburantExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'CHAUFFEURE',
            'DATE',
            'VIGNETTE',
            'MONTANT',
            'OBSERVATION',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Carburant::with('vehicule','chauffeure')->get();

    }
    
    public function map($Carburant): array
    {
        if (!$Carburant->montant) {
            $dh = "0";
        }else{
            $dh = $Carburant->montant;
        }

        return [
            $Carburant->id,
            $Carburant->vehicule->immatriculation . " " . $Carburant->vehicule->marque,
            $Carburant->chauffeure->nom . " " . $Carburant->chauffeure->prenom,
            $Carburant->date,
            $Carburant->Vignette,
            $dh . " DH",
            $Carburant->Observation,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:G1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Carburants'; 
    }

}
