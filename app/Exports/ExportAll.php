<?php

namespace App\Exports;

use App\Models\VTT;
use App\Models\mission;
use App\Models\vehicule;
use App\Models\Vignette;
use App\Models\Carburant;
use App\Models\Assaurance;
use App\Models\chauffeure;
use App\Models\Entretiens;
use App\Models\Reparation;
use App\Models\VisiteTechnique;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExportAll implements FromCollection, WithMultipleSheets
{
    use Exportable;

    public function sheets(): array
    {
        $sheets = []; 

        $sheets[] = new MissionExport;
        $sheets[] = new VehiculeExport;
        $sheets[] = new ChauffeureExport;
        $sheets[] = new CarburantExport;
        $sheets[] = new AssauranceExport;
        $sheets[] = new EntretiensExport;
        $sheets[] = new ReparationExport;
        $sheets[] = new VignetteExport;
        $sheets[] = new VisiteTechniqueExport;
        $sheets[] = new CarnetCarburantExport;
        $sheets[] = new VTTExport;

        return $sheets;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return Assaurance::all();
        // return Carburant::all();
        // return chauffeure::all();
        // return Entretiens::all();
        // return mission::all();
        // return Reparation::all();
        // return vehicule::all();
        // return Vignette::all();
        // return VisiteTechnique::all();
        // return VTT::all();
    }
}
