<?php

namespace App\Exports;

use App\Models\vehicule;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class VehiculeExport implements FromCollection,
ShouldAutoSize,
WithHeadings,
WithMapping,
WithEvents,
WithTitle
{
    use Exportable;

    public function Headings(): array
    {
        return [
            '#',
            'IMMATRICULATION',
            'TYPE',
            'MARQUE',
            'PUISSANCE',
            'DATE MISE EN ROUTE',
            'N CHASSIS',
            'CARBURAN',
            'ETABLISSEMENT',
            'NBR DE PLACE',
            'USAGE',
            'OBSERVATION',
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // $vehicule = vehicule::with('Mission')->first();
        
        // dd($vehicule);
        return vehicule::all();

        // return collect(vehicule::getvehicules());
    }

    public function map($vehicule): array
    {
        return [
            $vehicule->id,
            $vehicule->immatriculation,
            $vehicule->type,
            $vehicule->marque,
            $vehicule->puissance,
            $vehicule->date_mise_en_route,
            $vehicule->N_Chassis,
            $vehicule->Carburan, 
            $vehicule->etablissement,
            $vehicule->Nbre_de_place .' places',
            $vehicule->Usage,
            $vehicule->Observation,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H','I','J','K','L'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:L1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }


    public function fffff(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {

                
                // $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(7);
                // $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(5);
                // $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(5);  
                // $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(5);  
                // $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(5);  
                // $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(4);  
                // $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(10);  
                // $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(8);  
                // $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(20);  
                // $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(3);  
                // $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(8);  
                // $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(30);  

                // $event->sheet->getStyle('A1:L1')->applyFromArray([
                //     'font' => [
                //         'bold' => true
                //     ],
                //     'borders' => [
                //         'outline' => [
                //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                //             'color' => ['argb' => 'FFFF0000'],
                //         ],
                //     ]
                // ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Vehicules'; 
    }
}
