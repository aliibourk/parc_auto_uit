<?php

namespace App\Exports;

use App\Models\Assaurance;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AssauranceExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            "ENTREPRISE D'ASSAURANCE",
            'DATE DEBUT',
            'DUREE',
            'MONTANT',
        ];
    }

    public function map($assaurance): array
    {
        if (!$assaurance->montant) {
            $dh = "0";
        }else{
            $dh = $assaurance->montant;
        }

        return [
            $assaurance->id,
            $assaurance->vehicule->immatriculation . " " . $assaurance->vehicule->marque,
            $assaurance->Entreprise_assaurance->NomAssaurance,
            $assaurance->date_debut,
            $assaurance->duree . " Mois",
            $dh . " DH",
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:F1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Assaurances'; 
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Assaurance::all();
        
    }
}
