<?php

namespace App\Exports;

use App\Models\VTT;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VTTExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{

    public function Headings(): array
    {
        return [
            '#',
            'N BON',
            'TRANSPORTEUR',
            'BENEFICIARE',
            'DE',
            'A',
            'Montant',
            'DATE',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return VTT::withTrashed()->get();
    }

    public function map($Vtt): array
    {
        if (!$Vtt->Montant) {
            $dh = "0";
        }else{
            $dh = $Vtt->Montant;
        }

        return [
            $Vtt->id,
            $Vtt->N_Bon,
            $Vtt->Transporteur,
            $Vtt->Beneficiare,
            $Vtt->From,
            $Vtt->To,
            $dh . " DH",
            $Vtt->date,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'VTT'; 
    }



}
