<?php

namespace App\Exports;

use App\Models\mission;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class MissionExport implements FromCollection,ShouldAutoSize,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
WithColumnWidths
{

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 20,            
        ];
    }

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'CHAUFFEURE',
            'DATE DEBUT',
            'DATE FIN',
            'MONTANT',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Mission::with('vehicule','chauffeure')->get();
    }
    
    public function map($mission): array
    {
        if (!$mission->montant) {
            $dh = "0";
        }else{
            $dh = $mission->montant;
        }

        return [ 
            $mission->id,
            $mission->vehicule->immatriculation . " " . $mission->vehicule->marque,
            $mission->chauffeure->nom . " " . $mission->chauffeure->prenom,
            $mission->date_debut,
            $mission->date_fin,
            $dh . " DH",
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }

                $event->sheet->getStyle('A1:F1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },

        ];
    }

    public function title(): string
    {
        return 'Missions'; 
    }

}
