<?php

namespace App\Exports;

use App\Models\garage;
use App\Models\Carburant;
use App\Models\Entretiens;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet; 
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EntretiensExport implements FromCollection,
WithHeadings,
WithMapping,
WithEvents,
WithTitle,
ShouldAutoSize
{ 

    public function Headings(): array
    {
        return [
            '#',
            'VEHICULE',
            'CHAUFFEURE',
            'GARAGE',
            'N SOUCHE',
            'DATE',
            'OBJECT',
            'KELOMETRAGE',
            'MONTANT',
            'OBSERVATION',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Entretiens::with('vehicule','garage','chauffeure')->get();


        // dd(Entretiens::with('vehicule','garage','chauffeure')->get());

    }
    
    public function map($Entretien): array
    {
        $Garage = garage::where('id', $Entretien->Garage_id)->first();

        if (!$Entretien->montant_Carburant) {
            $dh = "0";
        }else{
            $dh = $Entretien->montant_Carburant;
        }
                
        return [
            $Entretien->id,
            $Entretien->vehicule->immatriculation . " " . $Entretien->vehicule->marque,
            $Entretien->chauffeure->nom . " " . $Entretien->chauffeure->prenom,
            $Garage->NomGarage,
            $Entretien->N_Souche,
            $Entretien->date,
            $Entretien->object,
            $Entretien->kelometrage . ' K',
            $dh . ' DH',
            $Entretien->Observation,
        ];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function(AfterSheet $event) {            

                $columns = ['A','B','C','D','E','F','G','H','I','J'];

                foreach ($columns as $column) {
                    $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);
                }
                
                $event->sheet->getStyle('A1:J1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],

                ]);
            },
        ];
    }

    public function title(): string
    {
        return 'Entretiens'; 
    }

}
