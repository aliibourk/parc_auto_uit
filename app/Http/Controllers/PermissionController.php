<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        return view('permissions')
        ->with('Permissions', Permission::all())
        ->with('Roles', Role::where('name','Staff')->get());
    }

    public function togglePrmission($r,$p)
    {
        $role = Role::findById($r);
        $Per = Permission::findById($p);

        if ($role->hasPermissionTo($Per->name))
        {
            $role->revokePermissionTo($Per->name); 
        }
        else
        {
            $role->givePermissionTo($Per->name);  
  
        }

        return back()
        ->with('Permissions', Permission::all())
        ->with('Roles', Role::all());
    
    }

}
