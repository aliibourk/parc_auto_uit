<?php

namespace App\Http\Controllers;

use Excel;
use App\Models\vehicule; 
use App\Exports\ExportAll;
use Illuminate\Http\Request;
use App\Exports\VehiculeExport;
use Illuminate\Support\Facades\Auth;

class VehiculeController extends Controller
{
    public function exportexcel(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('export')) {
            abort(403);
        }else{
            $date_export =  date('Y-m-d H:i');
            return (new ExportAll)->download('Parc Auto UIT '. $date_export .'.xlsx');
            // return (new ExportAll)->download('Export_Parc Auto UIT.xlsx');
            return redirect()->route('/mission');
        }
    }
    // public function exportCSV(){
    //     return (new ExportAll)->download('csv.csv');
    //     return redirect()->route('/mission');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select vehicule', Vehicule::class)) {
            abort(403);
        }

        $archive = false;
        return view('Vehicules-view.index', [
            'Vehicules' => Vehicule::all()->sortByDesc("updated_at")
        ])->with('TypeList', 'LISTE DES VEHICULES')->with('archive', $archive);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('select vehicule', Vehicule::class)) {
            abort(403);
        }

        $archive = true;
        return view('Vehicules-view.index', [
            'Vehicules' => Vehicule::onlyTrashed()->orderBy('updated_at')->get()

        ])->with('TypeList', "L'ARCHIVE DES VEHICULES")->with('archive', $archive);
            
    }

    public function restore(Request $request,$id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('restore vehicule', Vehicule::class)) {
            abort(403);
        }
        $vehicule = vehicule::onlyTrashed()->where('id',$id)->first();

        $vehicule->restore();

        $message="Une Véhicule a été restauré avec succès";
        return redirect()->back()->with('success',$message); 
    }

    public function forcedelete(Request $request,$id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('forcedelete vehicule', Vehicule::class)) {
            abort(403);
        }

        $vehicule = vehicule::onlyTrashed()->where('id',$id)->first();

        $vehicule->forceDelete();
        
        $message="Une Véhicule a été supprimer définitivement";
        return redirect()->back()->with('danger',$message); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('create vehicule', Vehicule::class)) {
            abort(403);
        }

        return view('Vehicules-view.create');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(StoreVehicule $request)
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('create vehicule', Vehicule::class)) {
            abort(403);
        }
        $validatedData = $request->validate([
            'immatriculation' => 'bail|string|unique:vehicules',
            'Usage' => 'bail|string|between:1,255|nullable',
            'marque' => 'bail|string|between:1,255',
            'N_Chassis' => 'bail|string|between:1,255|nullable',
            'etablissement' => 'bail|string|between:1,255|nullable',
            'puissance' => 'bail|numeric|gt:0|between:1,100000|nullable',
            'Nbre_de_place' => 'bail|numeric|gt:0|between:1,100|nullable',
            'Observation' => 'bail|string|max:200|nullable'
        ]);

        $data = $request->only([
            'immatriculation',
            'type',
            'marque',
            'puissance',
            'Carburan',
            'Usage',
            'N_Chassis',
            'date_mise_en_route',
            'etablissement',
            'Nbre_de_place',
            'Observation',
        ]);

        $vehicule = Vehicule::create($data);

        $message="Une Véhicule a été créé avec succès";
        return redirect()->route('vehicules.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return view('Vehicules-view.show', [
        //     'Vehicule' => Vehicule::findOrFail($id)
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit vehicule', Vehicule::class)) {
            abort(403);
        }
        $Vehicule = Vehicule::findOrFail($id);

        return view('Vehicules-view.edit', [
            'Vehicule' => $Vehicule
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit vehicule', Vehicule::class)) {
            abort(403);
        }

        $validatedData = $request->validate([
            'etablissement' => 'bail|string|between:1,255|nullable',
            'Usage' => 'bail|string|between:1,255|nullable',
            'Observation' => 'string|max:200|nullable'
        ]);


        $Vehicule = Vehicule::findOrFail($id);

        $Vehicule->Usage = $request->input('Usage');
        $Vehicule->etablissement = $request->input('etablissement');
        $Vehicule->Observation = $request->input('Observation');

        $Vehicule->save();

        $message="Une Véhicule a été modifier avec succès";
        return redirect()->route('vehicules.index')->with('primary',$message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('delete vehicule', Vehicule::class)) {
            abort(403);
        }

        $Vehicule = Vehicule::findOrFail($id);
        $Vehicule->delete();

        $message="Une Véhicule a été supprimer avec succès";
        return redirect()->route('vehicules.index')->with('danger',$message);
    }
}
