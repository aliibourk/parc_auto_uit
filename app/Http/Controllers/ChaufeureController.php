<?php

namespace App\Http\Controllers;
use App\Models\chauffeure; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChaufeureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select chauffeure')) {
            abort(403);
        }

        $archive = false;
        return view('Chauffeure-view.index', [
            'Chauffeures' => Chauffeure::all()
        ])->with('TypeList', 'LISTE DES Chauffeures')->with('archive', $archive);
    }


    public function archive(Request $request) 
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select chauffeure')) {
            abort(403);
        }

        $archive = true;
        return view('Chauffeure-view.index', [
            'Chauffeures' => Chauffeure::onlyTrashed()->orderBy('updated_at')->get()

        ])->with('TypeList', "L'ARCHIVE DES Chauffeures")->with('archive', $archive);
            
    }

    public function restore(Request $request,$id) 
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('restore chauffeure')) {
            abort(403);
        }
        
        $vehicule = Chauffeure::onlyTrashed()->where('id',$id)->first();

        $vehicule->restore();

        $message="Une Chauffeure a été restauré avec succès";
        return redirect()->back()->with('success',$message); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create chauffeure')) {
            abort(403);
        }
        
        return view('Chauffeure-view.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create chauffeure')) {
            abort(403);
        }
        
        $validatedData = $request->validate([
            'CIN' => 'bail|unique:chauffeures|between:5,10',
            'nom' => 'bail|max:30',
            'prenom' => 'bail|max:30',
            'telephone' => 'bail|numeric|unique:chauffeures|digits:10|starts_with:06,05,07|nullable'
            // 'telephone' => 'bail|max:10|nullable''integer|size:10';
        ]);

        $data = $request->only([
            'CIN',
            'nom',
            'prenom',
            'telephone',
            'disposible',
        ]);

        $Chauffeure = Chauffeure::create($data);

        $message="Un Chauffeure a été créé avec succès";

        return redirect()->route('chaufeures.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit chauffeure')) {
            abort(403);
        }
        
        $Chauffeure = Chauffeure::findOrFail($id);

        return view('Chauffeure-view.edit', [
            'Chauffeure' => $Chauffeure
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit chauffeure')) {
            abort(403);
        }
        
        $validatedData = $request->validate([
            'telephone' => 'bail|numeric|digits:10|starts_with:06,05,07|nullable'
        ]);

        $Chauffeure = Chauffeure::findOrFail($id);

        $Chauffeure->telephone = $request->input('telephone');
        $Chauffeure->disposible = $request->input('disposible');

        $Chauffeure->save();

        $message="Un Chauffeure a été modifier avec succès";
        return redirect()->route('chaufeures.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('delete chauffeure')) {
            abort(403);
        }
        
        $Chauffeure = Chauffeure::findOrFail($id);
        $Chauffeure->delete();

        $message="Un Chauffeure a été supprimer avec succès";

        return redirect()->route('chaufeures.index')->with('danger',$message);
    }
}
