<?php

namespace App\Http\Controllers;

use App\Models\garage;
use App\Models\vehicule;
use App\Models\chauffeure;
use Illuminate\Http\Request;
use App\Models\CarnetCarburant;
use App\Models\VisiteTechnique;
use Illuminate\Support\Facades\Auth;

class VisiteTechniqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select visite')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();
        $liste_chauffeure = chauffeure::withTrashed()->get();
        $liste_garage = garage::all();

        return view('VisiteTechnique-view.index', [
            'Visites' => VisiteTechnique::all()
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure)->with('liste_garage', $liste_garage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create visite')) {
            abort(403);
        }

        $all_carnet_id = CarnetCarburant::all()->pluck('vehicule_id')->toArray();
        $liste_vehicule = vehicule::all()->whereIn('id', $all_carnet_id);

        $liste_chauffeure = chauffeure::all();
        $liste_garage = garage::all();
        return view('VisiteTechnique-view.create')->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure)->with('liste_garage', $liste_garage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create visite')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|exists:vehicules,id|numeric',
            'chauffeure_id' => 'bail|exists:chauffeures,id|numeric',
            'Garage_id' => 'bail|exists:garages,id|numeric',
            'date' => 'bail|date',
            'montant_vignette' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'next_Visite' => 'bail|numeric|gt:0|max:100',
            'object' => 'bail|min:1|max:100|nullable',
            'N_Souche' => 'bail|min:1|max:100|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $data = $request->only([
            'date',
            'chauffeure_id',
            'vehicule_id',
            'montant_vignette',
            'object',
            'next_Visite',
            'N_Souche',
            'kelometrage',
            'Garage_id',
            'observation',
        ]);

        //_______________________________________________________________________

        $vehicule_id = $request->input('vehicule_id');
        $all_carnet = CarnetCarburant::all();

        $check_statut_carnet = "not existe";
        foreach ($all_carnet as $carnet) {
            if ($carnet->vehicule_id == $vehicule_id) {
                $C = CarnetCarburant::findOrFail($carnet->id);
                if ($C->Npage_total > $C->Npage_consomed) {
                    VisiteTechnique::create($data);
                    $C->Npage_consomed = $C->Npage_consomed + 1;
                    $C->save();

                    if ($C->Npage_consomed >= $C->Npage_total) {
                        $C->delete();
                    }

                    $check_statut_carnet = "existe";
                } else {
                    $check_statut_carnet = "expered";
                }
            }
        } 

        if ($check_statut_carnet == "not existe") {
            $message="la véhicule n'a pas de carnet carburant";
            return back()->with('danger',$message);
        }
        if ($check_statut_carnet == "existe") {
            $message="Une Visite Technique a été créé avec succès";
            return redirect()->route('VisiteTechnique.index')->with('success',$message);
        }
        if ($check_statut_carnet == "expered") {
            $message="Le carnet de cette véhicule a été Expiré";
            return back()->with('danger',$message);
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit visite')) {
            abort(403);
        }

        $Visite = VisiteTechnique::findOrFail($id);

        $MyVehicule = vehicule::where('id', $Visite->vehicule_id)->first();
        $MyChauffeure = chauffeure::where('id', $Visite->chauffeure_id)->first();
        $MyGarage = garage::where('id', $Visite->Garage_id)->first();

        return view('VisiteTechnique-view.edit', [
            'Visite' => $Visite
        ])->with('MyVehicule', $MyVehicule)
        ->with('MyChauffeure', $MyChauffeure)
        ->with('MyGarage', $MyGarage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit visite')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'date' => 'bail|date',
            'montant_vignette' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'next_Visite' => 'bail|numeric|gt:0|max:100',
            'object' => 'bail|min:1|max:100|nullable',
            'N_Souche' => 'bail|min:1|max:100|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $Visite = VisiteTechnique::findOrFail($id);

        $Visite->date = $request->input('date');
        $Visite->montant_vignette = $request->input('montant_vignette');
        $Visite->object = $request->input('object');
        $Visite->next_Visite = $request->input('next_Visite');
        $Visite->N_Souche = $request->input('N_Souche');
        $Visite->kelometrage = $request->input('kelometrage');
        $Visite->observation = $request->input('observation');

        $Visite->save();

        $message="Une Visite Technique a été modifier avec succès";

        return redirect()->route('VisiteTechnique.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request, $id)
    {
        // $Visite = VisiteTechnique::findOrFail($id);
        // $Visite->delete();

        // $message="Une Visite Technique a été supprimer avec succès";
        // return redirect()->route('VisiteTechnique.index')->with('danger',$message);
    }
}
