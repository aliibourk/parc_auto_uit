<?php

namespace App\Http\Controllers;

use App\Models\garage;
use App\Models\vehicule;
use App\Models\chauffeure;
use App\Models\costgarage;
use App\Models\Entretiens;
use Illuminate\Http\Request;
use App\Models\costentretien;
use App\Models\CarnetCarburant;
use Illuminate\Support\Facades\Auth;

class EntretienController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select entretien')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();
        $liste_chauffeure = chauffeure::withTrashed()->get();
        $liste_garage = garage::all();

        return view('Entretien-view.index', [
            'Entretiens' => Entretiens::all(),
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure)->with('liste_garage', $liste_garage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create entretien')) {
            abort(403);
        }

        $all_carnet_id = CarnetCarburant::all()->pluck('vehicule_id')->toArray();
        $liste_vehicule = vehicule::all()->whereIn('id', $all_carnet_id);

        $liste_chauffeure = chauffeure::all();
        $liste_garage = garage::all();

        return view('Entretien-view.create')
            ->with('liste_vehicule', $liste_vehicule)
            ->with('liste_chauffeure', $liste_chauffeure)
            ->with('liste_garage', $liste_garage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create entretien')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|exists:vehicules,id|numeric',
            'chauffeure_id' => 'bail|exists:chauffeures,id|numeric',
            'Garage_id' => 'bail|exists:garages,id|numeric',
            'date' => 'bail|date',
            'montant_Carburant' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'N_Souche' => 'bail|min:1|max:40|nullable',
            'object' => 'bail|min:1|max:40|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $data = $request->only([
            'chauffeure_id',
            'vehicule_id',
            'Garage_id',
            'date',
            'montant_Carburant',
            'kelometrage',
            'N_Souche',
            'object',
            'observation',
        ]);

        //_______________________________________________________________________

        $vehicule_id = $request->input('vehicule_id');

        $all_carnet = CarnetCarburant::all();
 
        $check_statut_carnet = "not existe";
        foreach ($all_carnet as $carnet) {
            if ($carnet->vehicule_id == $vehicule_id) {
                $C = CarnetCarburant::findOrFail($carnet->id);
                if ($C->Npage_total > $C->Npage_consomed) {
                    $C->Npage_consomed = $C->Npage_consomed + 1;
                    $C->save();
                    $Entretien = Entretiens::create($data);

                    if ($C->Npage_consomed >= $C->Npage_total) {
                        $C->delete();
                    }
                    
                    $check_statut_carnet = "existe";
                } else {
                    $check_statut_carnet = "expered";
                }
            }
        }

        if ($check_statut_carnet == "not existe") {
            $message="La véhicule n'a pas de carnet carburant";
            return redirect()->route('Entretien.index')->with('danger',$message);
        }
        if ($check_statut_carnet == "existe") {
            $message="Une Entretien a été créé avec succès";
            return redirect()->route('Entretien.index')->with('success',$message);
        }
        if ($check_statut_carnet == "expered") {
            $message="Le carnet de cette véhicule a été Expiré";
            return back()->with('danger',$message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit entretien')) {
            abort(403);
        }

        $Entretien = Entretiens::findOrFail($id);

        $MyVehicule = vehicule::where('id', $Entretien->vehicule_id)->first();
        $MyChauffeure = chauffeure::where('id', $Entretien->chauffeure_id)->first();
        $MyGarage = garage::where('id', $Entretien->Garage_id)->first();

        return view('Entretien-view.edit', [
            'Entretien' => $Entretien,
        ])->with('MyVehicule', $MyVehicule)
            ->with('MyChauffeure', $MyChauffeure)
            ->with('MyGarage', $MyGarage);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit entretien')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'date' => 'bail|date',
            'montant_Carburant' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'N_Souche' => 'bail|min:1|max:40|nullable',
            'object' => 'bail|min:1|max:40|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $Entretien = Entretiens::findOrFail($id);

        $Entretien->date = $request->input('date');
        $Entretien->montant_Carburant = $request->input('montant_Carburant');
        $Entretien->object = $request->input('object');
        $Entretien->N_Souche = $request->input('N_Souche');
        $Entretien->kelometrage = $request->input('kelometrage');
        $Entretien->observation = $request->input('observation');

        $Entretien->save();
        $message="l'entretien a été modifier avec succès";

        return redirect()->route('Entretien.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request, $id)
    {
        // $Entretien = Entretiens::findOrFail($id);
        // $Entretien->delete();

        // $message="l'entretien a été supprimer avec succès";

        // return redirect()->route('Entretien.index')->with('danger',$message);
    }
}
