<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\vehicule;
use App\Models\Assaurance;
use Illuminate\Http\Request;
use App\Models\CarnetCarburant;
use Illuminate\Support\Facades\Auth;
use App\Models\Entreprise_assaurance;

class AssauranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('select assaurance')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();

        $liste_assaurance = Entreprise_assaurance::all();

        return view('Assaurance-view.index', [
            'Assaurances' => Assaurance::all()
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_assaurance', $liste_assaurance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('create assaurance')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all()->whereNotIn('id', Assaurance::all()->pluck('vehicule_id')->toArray());

        $liste_assaurance = Entreprise_assaurance::all();

        return view('Assaurance-view.create')->with('liste_vehicule', $liste_vehicule)->with('liste_assaurance', $liste_assaurance);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('create assaurance')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|unique:assaurances|exists:vehicules,id|numeric',
            'entreprise_assaurance_id' => 'bail|numeric|exists:entreprise_assaurances,id',
            'date_debut' => 'date',
            'duree' => 'bail|numeric|between:1,120',
            'montant' => 'bail|numeric|between:1,9999999|nullable',
        ]);


        $data = $request->only([
            'vehicule_id',
            'entreprise_assaurance_id',
            'date_debut',
            'duree',
            'montant',
        ]);

        $Assaurance = Assaurance::create($data);
        $message="Une assaurance a été créé avec succès";

        return redirect()->route('Assaurance.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }

        if ($request->user()->cannot('edit assaurance')) {
            abort(403);
        }

        $Assaurance = Assaurance::findOrFail($id);
        $vehicule = vehicule::where('id', $Assaurance->vehicule_id)->first();

        $assaur = Entreprise_assaurance::where('id', $Assaurance->entreprise_assaurance_id)->first();

        return view('Assaurance-view.edit', [
            'Assaurance' => $Assaurance
        ])->with('vehicule', $vehicule)->with('assaur', $assaur);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit assaurance')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'date_debut' => 'date',
            'duree' => 'bail|numeric|between:1,120',
            'montant' => 'bail|numeric|between:1,9999999||nullable',
        ]);

        $Assaurance = Assaurance::findOrFail($id);

        // $Assaurance->vehicule_id = $request->input('vehicule_id');
        // $Assaurance->entreprise_assaurance_id = $request->input('entreprise_assaurance_id');
        $Assaurance->date_debut = $request->input('date_debut');
        $Assaurance->duree = $request->input('duree');
        $Assaurance->montant = $request->input('montant');

        $Assaurance->save();
        $message="Une Assaurance a été modifier avec succès";

        return redirect()->route('Assaurance.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // $Assaurance = Assaurance::findOrFail($id);
        // $Assaurance->delete();
        // $message="Une assaurance a été supprimer avec succès";
        
        // return redirect()->route('Assaurance.index')->with('danger',$message);
    }
}
