<?php

namespace App\Http\Controllers;

use App\Models\vehicule;
use App\Models\Carburant;
use App\Models\chauffeure;
use Illuminate\Http\Request;
use App\Models\CarnetCarburant;
use Illuminate\Support\Facades\Auth;


class CarburantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select carburant')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();

        $liste_chauffeure = chauffeure::withTrashed()->get();

        return view('carburant-view.index', [
            'Carburants' => Carburant::all()
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create carburant')) {
            abort(403);
        }

        $all_carnet_id = CarnetCarburant::all()->pluck('vehicule_id')->toArray();
        $liste_vehicule = vehicule::all()->whereIn('id', $all_carnet_id);

        $liste_chauffeure = chauffeure::all();

        return view('carburant-view.create')->with('liste_vehicule', $liste_vehicule)
            ->with('liste_chauffeure', $liste_chauffeure);
            // ->with('liste_Carnet', $liste_Carnet);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create carburant')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|exists:vehicules,id|numeric',
            'chauffeure_id' => 'bail|exists:chauffeures,id|numeric',
            'montant' => 'bail|numeric|min:1|max:100000|nullable',
            'Observation' => 'bail|max:200',
            'Vignette' => 'bail|max:50',
            'date' => 'bail|date',
        ]);

        $data = $request->only([
            'vehicule_id',
            'chauffeure_id',
            'date',
            'Vignette',
            'Observation',
            'montant',
        ]);

        //_______________________________________________________________________

        $vehicule_id = $request->input('vehicule_id');

        $all_carnet = CarnetCarburant::all();

        $check_statut_carnet = "not existe";
        foreach ($all_carnet as $carnet) {
            if ($carnet->vehicule_id == $vehicule_id) {
                $C = CarnetCarburant::findOrFail($carnet->id);
                if ($C->Npage_total > $C->Npage_consomed) {
                    $C->Npage_consomed = $C->Npage_consomed + 1;
                    $C->save();
                    $Carburant = Carburant::create($data);

                    if ($C->Npage_consomed >= $C->Npage_total) {
                        $C->delete();
                    }
                    
                    $check_statut_carnet = "existe";
                } else {
                    $check_statut_carnet = "expered";
                }
            }
        }

        if ($check_statut_carnet == "not existe") {
            $message="la véhicule n'a pas de carnet carburant";
            return redirect()->route('Carburant.index')->with('danger',$message);
        }
        if ($check_statut_carnet == "existe") {
            $message="Une Carburant a été créé avec succès";
            return redirect()->route('Carburant.index')->with('success',$message);
        }
        if ($check_statut_carnet == "expered") {
            $message="Le carnet de cette véhicule a été Expiré";
            return back()->with('danger',$message);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit carburant')) {
            abort(403);
        }

        $Carburant = Carburant::findOrFail($id);

        $MyVehicule = vehicule::where('id', $Carburant->vehicule_id)->first();
        $MyChauffeure = chauffeure::where('id', $Carburant->chauffeure_id)->first();

        // dd($MyVehicule);

        return view('Carburant-view.edit', [
            'Carburant' => $Carburant
        ])->with('MyVehicule', $MyVehicule)
            ->with('MyChauffeure', $MyChauffeure);
            // ->with('liste_Carnet', $liste_Carnet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit carburant')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'montant' => 'bail|numeric|min:1|max:100000|nullable',
            'Observation' => 'bail|max:200',
            'Vignette' => 'bail|max:50',
            'date' => 'bail|date',
        ]);

        $Carburant = Carburant::findOrFail($id);

        // $Carburant->vehicule_id = $request->input('vehicule_id');
        // $Carburant->chauffeure_id = $request->input('chauffeure_id');
        $Carburant->date = $request->input('date');
        $Carburant->Vignette = $request->input('Vignette');
        $Carburant->Observation = $request->input('Observation');
        $Carburant->montant = $request->input('montant');

        $Carburant->save();

        $message="Cette Carburant a été modifier avec succès";
        return redirect()->route('Carburant.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // $Carburant = Carburant::findOrFail($id);
        // $Carburant->delete();

        // $message="la Carburant a été supprimer avec succès";

        // return redirect()->route('Carburant.index')->with('danger',$message);
    }
}
