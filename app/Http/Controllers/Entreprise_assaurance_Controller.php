<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Entreprise_assaurance;


class Entreprise_assaurance_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select entreAss')) {
            abort(403);
        }
        return view('EntreAss-view.index', [
            'EntreAss' => Entreprise_assaurance::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create entreAss')) {
            abort(403);
        }
        return view('EntreAss-view.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create entreAss')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'NomAssaurance' => 'bail|unique:entreprise_assaurances|between:2,40',
            'Address' => 'bail|max:40',
            'Telephone' => 'bail|unique:entreprise_assaurances|numeric|digits:10|starts_with:06,05,07|nullable'
        ]);

        $data = $request->only([
            'NomAssaurance',
            'Address',
            'Telephone',
        ]);
        $Entreprise_assaurance = Entreprise_assaurance::create($data);

        $message="l'entreprise a été ajouter avec succès";

        return redirect()->route('Entreprise_assaurance.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $idEntreprise_assaurance
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit entreAss')) {
            abort(403);
        }
        $EntreAss = Entreprise_assaurance::findOrFail($id);

        return view('EntreAss-view.edit', [
            'EntreAss' => $EntreAss
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit entreAss')) {
            abort(403);
        }
        $validatedData = $request->validate([
            'Address' => 'bail|max:40',
            'Telephone' => 'bail|numeric|digits:10|starts_with:06,05,07|nullable'

        ]);

        $EntreAss = Entreprise_assaurance::findOrFail($id);

        // $EntreAss->NomAssaurance = $request->input('NomAssaurance');
        $EntreAss->Address = $request->input('Address');
        $EntreAss->Telephone = $request->input('Telephone');

        $EntreAss->save();

        
        $message="l'entreprise a été modifier avec succès";

        return redirect()->route('Entreprise_assaurance.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // $EntreAss = Entreprise_assaurance::findOrFail($id);
        // $EntreAss->delete();

        // $message="l'entreprise a été supprimer avec succès";

        // return redirect()->route('Entreprise_assaurance.index')->with('danger',$message);
    }
}
