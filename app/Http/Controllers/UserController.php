<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\chauffeure;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class UserController extends Controller
{

    use RegistersUsers;

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        $users = User::whereHas(
            'roles',
        function ($key) {
            $key->where('name', 'Staff');
        }
        )->get();

        return view('User-view.index', [
            'Users' => $users
            // 'Users' => User::where('active', 1)
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        return view('User-view.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
             
        $validatedData = $request->validate([
            'name' => 'bail|required|string|max:255|unique:users',
            'email' => 'bail|required|string|email|max:255|unique:users',
            'password' => 'required|string|min:4||max:255',
            'password_confirmation' => 'same:password'
        ]);

        $User = new User();
        $User->name = $request->input('name');
        $User->email = $request->input('email');
        $User->password =  Hash::make($request->input('password'));

        $User->save();

        User::where('email', '=', $request->email)->first()->assignRole('Staff');

        $message="Une Staff a été créé avec succès";

        return redirect()->route('Users.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        $user = User::findOrFail($id);

        if ($user->hasrole('Staff')) {
            return view('User-view.edit', [
                'User' => $user
            ]);
        } else {
            abort(404);
        }       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        $this->validate($request, [
            'Status' => 'required',
        ]);
        
        $user = User::findOrFail($id);

        $user->Status = $request->input('Status');

        $user->save();

        return redirect()->route('Users.index')
        ->with('success',"User : " . $user->name ." a été modifier avec succès");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        User::findOrFail($id)->delete();
        return redirect()->route('Users.index')
                        ->with('success','User a été supprimer avec succès');
    }
}
