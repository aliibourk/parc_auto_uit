<?php

namespace App\Http\Controllers;

use App\Models\VTT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VttController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('select vtt')) {
            abort(403);
        }

        return view('VTT-view.index', [
            'VTTs' =>  VTT::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('create vtt')) {
            abort(403);
        }

        return view('VTT-view.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('create vtt')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'N_Bon' => 'bail|unique:v_t_t_s|max:200',
            'Transporteur' => 'bail|max:200',
            'Beneficiare' => 'bail|max:200',
            'From' => 'bail|max:200|nullable',
            'To' => 'bail|min:1|max:200|nullable',
            'Montant' => 'numeric|gt:0|between:1,100000'
        ]);

        $data = $request->only([
            'N_Bon',
            'Transporteur',
            'Beneficiare',
            'From',
            'To',
            'Montant',
        ]);

        VTT::create($data);

        $message="Une Bon VTT a été créé avec succès";
        return redirect()->route('VTT.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('edit vtt')) {
            abort(403);
        }

        $VTT = VTT::findOrFail($id);
        
        return view('VTT-view.edit', [
            'VTT' => $VTT
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('edit vtt')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'N_Bon' => 'bail|unique:v_t_t_s|max:200',
            'Transporteur' => 'bail|max:200',
            'Beneficiare' => 'bail|max:200',
            'From' => 'bail|max:200|nullable',
            'To' => 'bail|min:1|max:200|nullable',
            'Montant' => 'numeric|gt:0|between:1,100000'
        ]);

        $VTT = VTT::findOrFail($id);

        // $VTT->N_Bon = $request->input('N_Bon');
        $VTT->Transporteur = $request->input('Transporteur');
        $VTT->Beneficiare = $request->input('Beneficiare');
        $VTT->From = $request->input('From');
        $VTT->To = $request->input('To');
        $VTT->Montant = $request->input('Montant');

        $VTT->save();

        $message="Une Bon VTT a été modifier avec succès";
        return redirect()->route('VTT.index')->with('primary',$message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request,$id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
      
        if ($request->user()->cannot('delete vtt')) {
            abort(403);
        }

        $VTT = VTT::findOrFail($id);
        $VTT->delete();

        $message="Une Bon VTT a été supprimer avec succès";
        return redirect()->route('VTT.index')->with('danger',$message);
    }
}
