<?php

namespace App\Http\Controllers;

use App\Models\vehicule;
use App\Models\Vignette;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VignetteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select vignette')) {
            abort(403);
        }

        $liste_vehicule =  vehicule::all();

        return view('Vignette-view.index', [
            'Vignettes' => Vignette::all()
        ])->with('liste_vehicule', $liste_vehicule);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create vignette')) {
            abort(403);
        }

        $Vehicule_libre = vehicule::all()
            ->whereNotIn('id', Vignette::all()
                ->pluck('vehicule_id')
                ->toArray());

        return view('Vignette-view.create')
            ->with('Vehicule_libre', $Vehicule_libre);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create vignette')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|numeric|unique:vignettes|exists:vehicules,id',
            'Date_Validite' => 'bail|date',
        ]);

        $data = $request->only([
            'vehicule_id',
            'Date_Validite',
        ]);

        $Vignette = Vignette::create($data);

        $message="Une Vignette a été créé avec succès";
        return redirect()->route('Vignette.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit vignette')) {
            abort(403);
        }

        $Vignette = Vignette::findOrFail($id);

        $MyVehicule = vehicule::where('id', $Vignette->vehicule_id)->first();

        return view('Vignette-view.edit', [
            'Vignette' => $Vignette
        ])->with('MyVehicule', $MyVehicule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit vignette')) {
            abort(403);
        }

        $Vignette = Vignette::findOrFail($id);
        $Vignette->Date_Validite = $request->input('Date_Validite');
        $Vignette->save();

        $message="Une Vignette a été modifier avec succès";
        return redirect()->route('Vignette.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // $Vignette = Vignette::findOrFail($id);
        // $Vignette->delete();

        // $message="Une Vignette a été supprimer avec succès";
        // return redirect()->route('Vignette.index')->with('danger',$message);
    }
}
