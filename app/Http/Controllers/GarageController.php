<?php

namespace App\Http\Controllers;

use App\Models\garage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GarageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select garage')) {
            abort(403);
        }

        return view('garage-view.index', [
            'garages' => garage::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create garage')) {
            abort(403);
        }
        
        return view('garage-view.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create garage')) {
            abort(403);
        }
        
        $validatedData = $request->validate([
            'NomGarage' => 'bail|unique:garages|between:2,40',
            'Address' => 'bail|max:40',
            // 'Telephone' => 'bail|numeric|unique:garages|digits:10|regex:/^(06)[0-9]{8}/|nullable'
            'Telephone' => 'bail|unique:garages|numeric|digits:10|starts_with:06,05,07|nullable'

        ]);

        $data = $request->only([
            'NomGarage',
            'Address',
            'Telephone',
        ]);
        $garage = garage::create($data);

        $message="Une garage a été créé avec succès";

        return redirect()->route('Garages.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit garage')) {
            abort(403);
        }
        
        $garage = garage::findOrFail($id);

        return view('garage-view.edit', [
            'garage' => $garage
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit garage')) {
            abort(403);
        }
        
        $validatedData = $request->validate([
            // 'NomGarage' => 'bail|unique:garages|between:2,40',
            'Address' => 'bail|max:40',
            // 'Telephone' => 'bail|numeric|digits:10|regex:/^(06)|(05)[0-9]{8}/|nullable'starts_with:foo,bar,...
            'Telephone' => 'bail|numeric|digits:10|starts_with:06,05,07|nullable'
        ]);

        $garage = garage::findOrFail($id);

        // $garage->NomGarage = $request->input('NomGarage');
        $garage->Address = $request->input('Address');
        $garage->Telephone = $request->input('Telephone');

        $garage->save();

        $message="Une garage a été modifier avec succès";
        return redirect()->route('Garages.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy(Request $request, $id)
    {
        // $Garage = Garage::findOrFail($id);
        // $Garage->delete();

        // $message="Une Garage a été supprimer avec succès";
        // return redirect()->route('Garages.index')->with('danger',$message);
    }
}
