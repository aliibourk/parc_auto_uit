<?php

namespace App\Http\Controllers;

use App\Models\garage;
use App\Models\vehicule;
use App\Models\chauffeure;
use App\Models\Reparation;
use Illuminate\Http\Request;
use App\Models\CarnetCarburant;
use Illuminate\Support\Facades\Auth;

class ReparationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select reparation')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();
        $liste_chauffeure = chauffeure::withTrashed()->get();
        $liste_garage = garage::all();

        return view('Reparation-view.index', [
            'Reparations' => Reparation::all()
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure)->with('liste_garage', $liste_garage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create reparation')) {
            abort(403);
        }

        $all_carnet_id = CarnetCarburant::all()->pluck('vehicule_id')->toArray();
        $liste_vehicule = vehicule::all()->whereIn('id', $all_carnet_id);
        
        $liste_chauffeure = chauffeure::all();
        $liste_garage = garage::all();

        return view('Reparation-view.create')->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure)->with('liste_garage', $liste_garage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create reparation')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|exists:vehicules,id|numeric',
            'chauffeure_id' => 'bail|exists:chauffeures,id|numeric',
            'Garage_id' => 'bail|exists:garages,id|numeric',
            'date' => 'bail|date',
            'montant_vignette' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'object' => 'bail|min:1|max:100|nullable',
            'N_Souche' => 'bail|min:1|max:100|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $data = $request->only([
            'date',
            'chauffeure_id',
            'vehicule_id',
            'montant_vignette',
            'object',
            'N_Souche',
            'kelometrage',
            'Garage_id',
            'observation',
        ]);

        //_______________________________________________________________________

        $vehicule_id = $request->input('vehicule_id');

        $all_carnet = CarnetCarburant::all();

        $check_statut_carnet = "not existe";
        foreach ($all_carnet as $carnet) {
            if ($carnet->vehicule_id == $vehicule_id) {
                $C = CarnetCarburant::findOrFail($carnet->id);
                if ($C->Npage_total > $C->Npage_consomed) {
                    $Reparation = Reparation::create($data);
                    $C->Npage_consomed = $C->Npage_consomed + 1;
                    $C->save();

                    if ($C->Npage_consomed >= $C->Npage_total) {
                        $C->delete();
                    }

                    $check_statut_carnet = "existe";
                } else {
                    $check_statut_carnet = "expered";
                }
            }
        }
 
        if ($check_statut_carnet == "not existe") {
            $message="la véhicule n'a pas de carnet carburant";
            return redirect()->route('Reparation.index')->with('danger',$message);
        }
        if ($check_statut_carnet == "existe") {
            $message="Une Reparation a été créé avec succès";
            return redirect()->route('Reparation.index')->with('success',$message);
        }
        if ($check_statut_carnet == "expered") {
            $message="Le carnet de cette véhicule a été Expiré";
            return back()->with('danger',$message);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,  $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit reparation')) {
            abort(403);
        }

        $Reparation = Reparation::findOrFail($id);

        $MyVehicule = vehicule::where('id', $Reparation->vehicule_id)->first();
        $MyChauffeure = chauffeure::where('id', $Reparation->chauffeure_id)->first();
        $MyGarage = garage::where('id', $Reparation->Garage_id)->first();


        return view('Reparation-view.edit', [
            'Reparation' => $Reparation
        ])->with('MyVehicule', $MyVehicule)
        ->with('MyChauffeure', $MyChauffeure)
        ->with('MyGarage', $MyGarage);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit reparation')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'date' => 'bail|date',
            'montant_vignette' => 'bail|numeric|gt:0|min:1|max:100000|nullable',
            'object' => 'bail|min:1|max:100|nullable',
            'N_Souche' => 'bail|min:1|max:100|nullable',
            'kelometrage' => 'bail|numeric|gt:0|nullable',
            'observation' => 'bail|max:200|nullable',
        ]);

        $Reparation = Reparation::findOrFail($id);

        $Reparation->date = $request->input('date');
        // $Reparation->chauffeure_id = $request->input('chauffeure_id');
        // $Reparation->vehicule_id = $request->input('vehicule_id');
        $Reparation->montant_vignette = $request->input('montant_vignette');
        $Reparation->object = $request->input('object');
        $Reparation->N_Souche = $request->input('N_Souche');
        $Reparation->kelometrage = $request->input('kelometrage');
        // $Reparation->Garage_id = $request->input('Garage_id');
        $Reparation->observation = $request->input('observation');

        $Reparation->save();

        $message="Une Reparation a été modifier avec succès";
        return redirect()->route('Reparation.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request, $id)
    {
        // $Reparation = Reparation::findOrFail($id);
        // $Reparation->delete();
        
        // $message="Une Reparation a été supprimer avec succès";
        // return redirect()->route('Reparation.index')->with('danger',$message);
    }
}
