<?php

namespace App\Http\Controllers;

use App\Models\vehicule;
use Illuminate\Http\Request;
use App\Models\CarnetCarburant;
use PhpParser\Node\Stmt\Foreach_;
use Illuminate\Support\Facades\Auth;

class CarnetCarburantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select carnet')) {
            abort(403);
        }

        $archive = false;
        $liste_vehicule = vehicule::all();

        $Carnets = CarnetCarburant::all();

        foreach($Carnets as $Carnet) {
            if ($Carnet->Npage_total <= $Carnet->Npage_consomed) {
                $Carnet->delete();
            }
        } 

        return view('CarnetCarburan-view.index', [
            'Carnet' =>  CarnetCarburant::all()
        ])->with('TypeList', "LISTE DES Carnets")
        ->with('liste_vehicule', $liste_vehicule)
        ->with('archive', $archive);
    }

    public function archive(Request $request) 
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select carnet')) {
            abort(403);
        }

        $archive = true;
        $Carnets = CarnetCarburant::all();
        $liste_vehicule = vehicule::withTrashed()->get();

        foreach($Carnets as $Carnet) {
            if ($Carnet->Npage_total <= $Carnet->Npage_consomed) {
                $Carnet->delete();
            }
        }

        return view('CarnetCarburan-view.index', [
            'Carnet' => CarnetCarburant::onlyTrashed()->orderBy('updated_at')->get()

        ])->with('TypeList', "L'ARCHIVE DES Carnets")
        ->with('liste_vehicule', $liste_vehicule)
        ->with('archive', $archive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create carnet')) {
            abort(403);
        }

        $Vehicule_libre = vehicule::all()->whereNotIn('id', CarnetCarburant::all()->pluck('vehicule_id')->toArray());

        return view('CarnetCarburan-view.create')
            ->with('Vehicule_libre', $Vehicule_libre);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create carnet')) {
            abort(403);
        }

        $count_carnet = CarnetCarburant::all()->where('vehicule_id',  $request->input('vehicule_id'));
        $n = $count_carnet->count();
        
        if ($n == 1) {

            $validatedData = $request->validate([
                'vehicule_id' => 'bail|unique:carnet_carburants|exists:vehicules,id|numeric',
                'debut' => 'bail|numeric|gt:0|between:1,99999999|',
                'fin' => 'numeric|gt:0|between:1,99999999|gt:debut'
            ]);

         } 
         if ($n == 0) {
              $validatedData = $request->validate([
                    'vehicule_id' => 'bail|exists:vehicules,id|numeric',
                    'debut' => 'bail|numeric|gt:0|between:1,99999999|',
                    'fin' => 'numeric|gt:0|between:1,99999999|gt:debut'
                ]);

         }

        $Carnet = new CarnetCarburant();
        $Carnet->vehicule_id = $request->input('vehicule_id');
        $Carnet->debut = $request->input('debut');
        $Carnet->fin = $request->input('fin');
        $Carnet->Npage_total = $request->input('fin') - $request->input('debut') + 1;

        $Carnet->save();

        $message="Une Carnet a été créé avec succès";

        return redirect()->route('CarnetCarburant.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $Carnet = CarnetCarburant::findOrFail($id);
        // $vehicule = vehicule::where('id', $Carnet->vehicule_id)->first();

        // return view('CarnetCarburan-view.edit', [
        //     'CarnetCarburant' => $Carnet
        // ])->with('vehicule', $vehicule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $validatedData = $request->validate([
        //     'debut' => 'bail|numeric|gt:0|between:1,99999999|',
        //     'fin' => 'numeric|gt:0|between:1,99999999|gt:debut',
        // ]);

        // $Carnet = CarnetCarburant::findOrFail($id);

        // $Carnet->debut = $request->input('debut');
        // $Carnet->fin = $request->input('fin');
        // $Carnet->Npage_consomed = 0 ;

        // $Carnet->save();

        // $message="le Carnet a été modifier avec succès";
        // return redirect()->route('CarnetCarburant.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
