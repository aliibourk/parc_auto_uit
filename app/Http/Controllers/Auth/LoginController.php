<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     protected function authenticated(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }else{
            return redirect('/home');
        }
    }

    // public function authenticate(Request $request)
    // {
    //     if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1])) {
    //         return redirect()->intended($this->redirectPath());
    //     }
    // }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
