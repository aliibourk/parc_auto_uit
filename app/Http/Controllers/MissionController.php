<?php

namespace App\Http\Controllers;

use App\Models\mission;
use App\Models\vehicule;
use App\Models\chauffeure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('select mission')) {abort(403);}

        $liste_vehicule = vehicule::all();
        $liste_chauffeure = chauffeure::withTrashed()->get();

        return view('Mission-view.index', [
            'Missions' => mission::all()
        ])->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create mission')) {
            abort(403);
        }

        $liste_vehicule = vehicule::all();
        $liste_chauffeure = chauffeure::all();

        return view('Mission-view.create')->with('liste_vehicule', $liste_vehicule)->with('liste_chauffeure', $liste_chauffeure);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('create mission')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'vehicule_id' => 'bail|exists:vehicules,id|numeric',
            'chauffeure_id' => 'bail|exists:chauffeures,id|numeric',
            'montant' => 'bail|numeric|between:1,100000|nullable',
            'date_debut' => 'bail|date',
            'date_fin' => 'bail|date|after_or_equal:date_debut',
        ]);

        $data = $request->only([
            'vehicule_id',
            'chauffeure_id',
            'date_debut',
            'date_fin',
            'montant',
        ]);

        $mission = Mission::create($data);

        $message="Une Mission a été créé avec succès";
        return redirect()->route('mission.index')->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $liste_vehicule = D::table('vehicules')
        //                     ->get();

        // $liste_chauffeure = B::table('chauffeures')
        //                     ->get();

        // return view('Mission-view.show', [
        //     'Mission' => Mission::findOrFail($id)
        // ])->with('liste_vehicule' , $liste_vehicule)->with('liste_chauffeure' , $liste_chauffeure);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit mission')) {
            abort(403);
        }

        $mission = Mission::findOrFail($id);

        $MyVehicule = vehicule::where('id', $mission->vehicule_id)->first();
        $MyChauffeure = chauffeure::where('id', $mission->chauffeure_id)->first();

        return view('Mission-view.edit', [
            'Mission' => $mission
        ])->with('MyVehicule', $MyVehicule)
            ->with('MyChauffeure', $MyChauffeure);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->Status) {
            Auth::logout();
            return redirect('/login')->with('danger', 'Votre compte est suspendu');
        }
        
        if ($request->user()->cannot('edit mission')) {
            abort(403);
        }

        $validatedData = $request->validate([
            'montant' => 'bail|numeric|between:1,100000|nullable',
            'date_debut' => 'bail|date',
            'date_fin' => 'bail|date|after_or_equal:date_debut',
        ]);

        $Mission = Mission::findOrFail($id);

        // $Mission->vehicule_id = $request->input('vehicule_id');
        // $Mission->chauffeure_id = $request->input('chauffeure_id');
        $Mission->date_debut = $request->input('date_debut');
        $Mission->date_fin = $request->input('date_fin');
        $Mission->montant = $request->input('montant');

        $Mission->save();

        $message="Une Mission a été modifier avec succès";
        return redirect()->route('mission.index')->with('primary',$message);
    }

    /**
     * Remove the specified resource from storage. 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // $Mission = Mission::findOrFail($id);
        // $Mission->delete();

        // $message="Une Mission a été supprimer avec succès";
        // return redirect()->route('mission.index')->with('danger',$message);
    }
}
