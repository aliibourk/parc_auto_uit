<?php

namespace App\Models;

use App\Models\mission;
use App\Models\vehicule;
use App\Models\CarnetCarburant;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class vehicule extends Model
{
    use HasFactory;
    use SoftDeletes; 

    protected $fillable = [
        'immatriculation',
        'type',
        'marque',
        'puissance',
        'Carburan',
        'Usage',
        'N_Chassis',
        'date_mise_en_route',
        'etablissement',
        'Nbre_de_place',
        'Observation',
    ];

    public function Assaurance(){ 
        return $this->hasOne(Assaurance::class);}

    public function Carburant(){ 
        return $this->hasMany(Carburant::class);}
        
    public function Carnet(){ 
        return $this->hasOne(CarnetCarburant::class);}

    public function Entretiens(){ 
        return $this->hasMany(Entretiens::class);}
 
    public function Mission(){ 
        return $this->hasMany(mission::class);}

    public function Reparation(){ 
        return $this->hasMany(Reparation::class);}

    public function Vignette(){ 
        return $this->hasOne(Vignette::class);}

    public function VisiteTechnique(){ 
        return $this->hasMany(VisiteTechnique::class);}
    
    
    
    
    public static function boot(){
        parent::boot();

        static::deleting(function(vehicule $vehicule){
            $vehicule->Assaurance()->delete();
            $vehicule->Carburant()->delete();
            $vehicule->Carnet()->delete();
            $vehicule->Entretiens()->delete();
            $vehicule->Mission()->delete();
            $vehicule->Reparation()->delete();
            $vehicule->Vignette()->delete();
            $vehicule->VisiteTechnique()->delete();
        });

        static::restoring(function(vehicule $vehicule){
            $vehicule->Assaurance()->restore();
            $vehicule->Carburant()->restore();
            $vehicule->Carnet()->restore();
            $vehicule->Entretiens()->restore();
            $vehicule->Mission()->restore();
            $vehicule->Reparation()->restore();
            $vehicule->Vignette()->restore();
            $vehicule->VisiteTechnique()->restore();
        });
    }




    public static function getvehicules()
    {

    }

    // public function vehicules()
    // {
    //     return $this->belongsToMany('App\Models\vehicule')->withTimestamps();
    // }
}
