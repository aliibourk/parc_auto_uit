<?php

namespace App\Models;

use App\Models\garage;
use App\Models\vehicule;
use App\Models\chauffeure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VisiteTechnique extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['date',
        'chauffeure_id',
        'vehicule_id',
        'next_Visite',
        'N_Souche',
        'montant_vignette',
        'object',
        'kelometrage',
        'Garage_id',
        'observation'
    ];

    public function vehicule() 
    {
        return $this->belongsTo(vehicule::class);
    }

    public function chauffeure() 
    {
        return $this->belongsTo(chauffeure::class);
    }
    
    public function garage() 
    {
        return $this->belongsTo(garage::class);
    }

}
