<?php

namespace App\Models;

use App\Models\vehicule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Carburant extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['vehicule_id',
    'chauffeure_id',
    'date',
    'Vignette',
    'Observation',
    'montant',];

    public function vehicule() 
    {
        return $this->belongsTo(vehicule::class);
    }

    public function chauffeure() 
    {
        return $this->belongsTo(chauffeure::class);
    }


}
