<?php

namespace App\Models;

use App\Models\vehicule;
use App\Models\Entreprise_assaurance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Assaurance extends Model
{  
    use HasFactory;
    use SoftDeletes;
    
    protected $fillable = [
        'vehicule_id',
        'entreprise_assaurance_id',
        'date_debut',
        'duree',
        'montant',
    ];


    public function vehicule() 
    {
        return $this->belongsTo(vehicule::class);
    }

    public function Entreprise_assaurance() 
    {
        return $this->belongsTo(Entreprise_assaurance::class);
    }
}
