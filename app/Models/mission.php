<?php

namespace App\Models;

use App\Models\vehicule;
use App\Models\chauffeure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class mission extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['vehicule_id',
                            'chauffeure_id',
                            'date_debut',
                            'date_fin',
                            'montant',];
                
    public function vehicule() 
    {
        return $this->belongsTo(vehicule::class);
    }

    public function chauffeure() 
    {
        return $this->belongsTo(chauffeure::class);
    }

}
