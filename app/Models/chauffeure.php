<?php

namespace App\Models;

use App\Models\mission;
use App\Models\Carburant;
use App\Models\Entretiens;
use App\Models\Reparation;
use App\Models\VisiteTechnique;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class chauffeure extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['CIN',
                            'nom',
                            'prenom',
                            'telephone',
                            'disposible'];


    public function Mission(){ 
        return $this->hasMany(mission::class);}

    public function Carburant(){ 
        return $this->hasMany(Carburant::class);}
            
    public function Entretiens(){ 
        return $this->hasMany(Entretiens::class);}

    public function Reparation(){ 
        return $this->hasMany(Reparation::class);}
    
    public function VisiteTechnique(){ 
        return $this->hasMany(VisiteTechnique::class);}
        

}
