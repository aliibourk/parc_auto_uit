<?php

namespace App\Models;

use App\Models\Assaurance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Entreprise_assaurance extends Model
{ 
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['NomAssaurance',
        'Address',
        'Telephone'];

    public function Assaurance() 
    {
        return $this->belongsTo(Assaurance::class);
    }
}
