<?php

namespace App\Models;

use App\Models\vehicule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CarnetCarburant extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['vehicule_id',
        'Date_creation',
        'Npage_total',
        'Npage_consomed',
        'debut',
        'fin'];

    public function vehicule()
    {
        return $this->belongsTo(vehicule::class);
    }

}
