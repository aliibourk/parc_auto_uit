<?php

namespace App\Models;

use App\Models\garage;
use App\Models\vehicule;
use App\Models\chauffeure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reparation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['date',
        'chauffeure_id',
        'vehicule_id',
        'montant_vignette',
        'object',
        'N_Souche',
        'kelometrage',
        'Garage_id',
        'observation'];



    public function vehicule() 
    {
        return $this->belongsTo(vehicule::class);
    }

    public function chauffeure() 
    {
        return $this->belongsTo(chauffeure::class);
    }
    
    public function garage() 
    {
        return $this->belongsTo(garage::class);
    }
    
}
