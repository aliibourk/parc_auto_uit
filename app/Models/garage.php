<?php

namespace App\Models;

use App\Models\Entretiens; 
use App\Models\Reparation;
use App\Models\VisiteTechnique;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class garage extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['NomGarage',
        'Address',
        'Telephone'
    ];


    public function Entretiens() 
    {
        return $this->hasMany(Entretiens::class);
    }

    public function Reparation() 
    {
        return $this->belongsTo(Reparation::class);
    }

    public function VisiteTechnique() 
    {
        return $this->belongsTo(VisiteTechnique::class);
    }


    
}
