<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vignette extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = ['vehicule_id',
        'Date_Validite',
    ];

    public function vehicule()
    {
        // return $this->belongsToMany('App\Models\vehicule')->withTimestamps();
        return $this->belongsTo(vehicule::class);
    }
}
