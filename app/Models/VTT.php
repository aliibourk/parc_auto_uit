<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VTT extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['Date',
        'N_Bon',
        'Transporteur',
        'Beneficiare',
        'From',
        'To',
        'Montant'];

}
