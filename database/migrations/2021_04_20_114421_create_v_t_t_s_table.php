<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVTTSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_t_t_s', function (Blueprint $table) {
            $table->id();
            $table->string('N_Bon')->unique();
            $table->string('Transporteur')->nullable();;
            $table->string('Beneficiare');
            $table->string('From')->nullable();;
            $table->string('To')->nullable();;
            $table->string('Montant');
            $table->date('date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_t_t_s');
    }
}
