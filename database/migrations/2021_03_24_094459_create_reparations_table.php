<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('reparations', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->foreignId('chauffeure_id')->constrained()->onDelete('cascade')->nullable();
            $table->foreignId('vehicule_id')->constrained()->onDelete('cascade');
            $table->integer('montant_vignette')->nullable();
            $table->string('N_Souche')->nullable();
            $table->string('object')->nullable();
            $table->float('kelometrage')->nullable();
            $table->foreignId('Garage_id')->constrained()->onDelete('cascade');
            $table->longText('observation')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparations');
    }
}
