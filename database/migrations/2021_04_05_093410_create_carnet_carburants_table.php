<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCarnetCarburantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carnet_carburants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicule_id')->constrained()->onDelete('cascade');
            $table->date('Date_creation')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('debut');
            $table->integer('fin');
            $table->integer('Npage_total');
            $table->integer('Npage_consomed')->default(0);

            // $table->integer('Montant');
            // $table->integer('Rest')->default('0');
            // $table->date('Date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carnet_carburants');
    }
}
