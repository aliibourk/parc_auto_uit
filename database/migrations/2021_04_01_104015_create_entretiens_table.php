<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntretiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entretiens', function (Blueprint $table) {
            $table->id();
            $table->string('N_Souche')->nullable();
            $table->foreignId('chauffeure_id')->constrained()->onDelete('cascade')->nullable();
            $table->foreignId('vehicule_id')->constrained()->onDelete('cascade');
            $table->foreignId('Garage_id')->constrained()->onDelete('cascade');
            $table->date('date');
            $table->string('object')->nullable();
            $table->float('kelometrage')->nullable();
            $table->integer('montant_Carburant')->nullable();
            $table->longText('observation')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entretiens');
    }
}
