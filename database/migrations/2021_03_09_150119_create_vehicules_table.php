<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {
            $table->id();
            $table->string('immatriculation')->unique();
            $table->string('type');
            $table->string('marque')->nullable();
            $table->string('puissance')->nullable();
            $table->date('date_mise_en_route')->nullable();
            $table->string('N_Chassis')->nullable();
            $table->enum('Carburan', ['Essence', 'diesel', 'electrique', 'none'])->nullable();
            $table->string('etablissement')->nullable();
            $table->integer('Nbre_de_place')->nullable();
            $table->string('Usage')->nullable();
            $table->text('Observation')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
