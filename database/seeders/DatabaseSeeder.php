<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();


        //craete roles
        (Role::where('name','Super Admin')->first()) ? : Role::create(['name' => 'Super Admin']) ;
        (Role::where('name','Admin')->first()) ? : Role::create(['name' => 'Admin']) ;
        (Role::where('name','Staff')->first()) ? : Role::create(['name' => 'Staff']) ;

        //craete Users
        (User::where('email','aliiboone999@gmail.com')->first()) ? : $user = User::create(
            [
                'name' => 'Ali', 
                'email' => 'aliiboone999@gmail.com',
                'password' => bcrypt('zahzaha'),
            ]
        );

        (User::where('email','aghad.mohamed@uit.ac.ma')->first()) ? : $user = User::create(
            [
                'name' => 'Mohamed Aghad', 
                'email' => 'aghad.mohamed@uit.ac.ma',
                'password' => bcrypt('12345678'),
            ]
        );

        // (User::where('email','Staff@gmail.com')->first()) ? : $user = User::create(
        //     [
        //         'name' => 'Staff', 
        //         'email' => 'Staff@gmail.com',
        //         'password' => bcrypt('Staff@gmail.com'),
        //     ]
        // );


        //assing roles 
        User::where('email', '=', 'aliiboone999@gmail.com')->first()->assignRole('Super Admin');
        User::where('email', '=', 'aghad.mohamed@uit.ac.ma')->first()->assignRole('Admin');


        //create permissions---------------------------------------------------------------------------------------

        // vehicule
        (Permission::where('name','select vehicule')->first()) ? : Permission::create(['name' => 'select vehicule']) ;
        (Permission::where('name','create vehicule')->first()) ? : Permission::create(['name' => 'create vehicule']) ;
        (Permission::where('name','edit vehicule')->first()) ? : Permission::create(['name' => 'edit vehicule']) ;
        (Permission::where('name','delete vehicule')->first()) ? : Permission::create(['name' => 'delete vehicule']) ;
        (Permission::where('name','restore vehicule')->first()) ? : Permission::create(['name' => 'restore vehicule']) ;
        (Permission::where('name','forcedelete vehicule')->first()) ? : Permission::create(['name' => 'forcedelete vehicule']) ;

        // chauffeure
        (Permission::where('name','select chauffeure')->first()) ? : Permission::create(['name' => 'select chauffeure']) ;
        (Permission::where('name','create chauffeure')->first()) ? : Permission::create(['name' => 'create chauffeure']) ;
        (Permission::where('name','edit chauffeure')->first()) ? : Permission::create(['name' => 'edit chauffeure']) ;
        (Permission::where('name','delete chauffeure')->first()) ? : Permission::create(['name' => 'delete chauffeure']) ;
        (Permission::where('name','restore chauffeure')->first()) ? : Permission::create(['name' => 'restore chauffeure']) ;

        // assaurance
        (Permission::where('name','select assaurance')->first()) ? : Permission::create(['name' => 'select assaurance']) ;
        (Permission::where('name','create assaurance')->first()) ? : Permission::create(['name' => 'create assaurance']) ;
        (Permission::where('name','edit assaurance')->first()) ? : Permission::create(['name' => 'edit assaurance']) ;

        // carburant
        (Permission::where('name','select carburant')->first()) ? : Permission::create(['name' => 'select carburant']) ;
        (Permission::where('name','create carburant')->first()) ? : Permission::create(['name' => 'create carburant']) ;
        (Permission::where('name','edit carburant')->first()) ? : Permission::create(['name' => 'edit carburant']) ;

        // carnet
        (Permission::where('name','select carnet')->first()) ? : Permission::create(['name' => 'select carnet']) ;
        (Permission::where('name','create carnet')->first()) ? : Permission::create(['name' => 'create carnet']) ;
        (Permission::where('name','edit carnet')->first()) ? : Permission::create(['name' => 'edit carnet']) ;

        // EntreAss
        (Permission::where('name','select entreAss')->first()) ? : Permission::create(['name' => 'select entreAss']) ;
        (Permission::where('name','create entreAss')->first()) ? : Permission::create(['name' => 'create entreAss']) ;
        (Permission::where('name','edit entreAss')->first()) ? : Permission::create(['name' => 'edit entreAss']) ;

        // Entretien
        (Permission::where('name','select entretien')->first()) ? : Permission::create(['name' => 'select entretien']) ;
        (Permission::where('name','create entretien')->first()) ? : Permission::create(['name' => 'create entretien']) ;
        (Permission::where('name','edit entretien')->first()) ? : Permission::create(['name' => 'edit entretien']) ;

         // garage
        (Permission::where('name','select garage')->first()) ? : Permission::create(['name' => 'select garage']) ;
         (Permission::where('name','create garage')->first()) ? : Permission::create(['name' => 'create garage']) ;
         (Permission::where('name','edit garage')->first()) ? : Permission::create(['name' => 'edit garage']) ;

         // mission
        (Permission::where('name','select mission')->first()) ? : Permission::create(['name' => 'select mission']) ;
         (Permission::where('name','create mission')->first()) ? : Permission::create(['name' => 'create mission']) ;
         (Permission::where('name','edit mission')->first()) ? : Permission::create(['name' => 'edit mission']) ;

        // reparation
        (Permission::where('name','select reparation')->first()) ? : Permission::create(['name' => 'select reparation']) ;
        (Permission::where('name','create reparation')->first()) ? : Permission::create(['name' => 'create reparation']) ;
        (Permission::where('name','edit reparation')->first()) ? : Permission::create(['name' => 'edit reparation']) ;

        // Vignette
        (Permission::where('name','select vignette')->first()) ? : Permission::create(['name' => 'select vignette']) ;
        (Permission::where('name','create vignette')->first()) ? : Permission::create(['name' => 'create vignette']) ;
        (Permission::where('name','edit vignette')->first()) ? : Permission::create(['name' => 'edit vignette']) ;

        // Visite
        (Permission::where('name','select visite')->first()) ? : Permission::create(['name' => 'select visite']) ;
        (Permission::where('name','create visite')->first()) ? : Permission::create(['name' => 'create visite']) ;
        (Permission::where('name','edit visite')->first()) ? : Permission::create(['name' => 'edit visite']) ;

        // vtt
        (Permission::where('name','select vtt')->first()) ? : Permission::create(['name' => 'select vtt']) ;
        (Permission::where('name','create vtt')->first()) ? : Permission::create(['name' => 'create vtt']) ;
        (Permission::where('name','edit vtt')->first()) ? : Permission::create(['name' => 'edit vtt']) ;
        (Permission::where('name','delete vtt')->first()) ? : Permission::create(['name' => 'delete vtt']) ;

        // exporte
        // (Permission::where('name','exporte')->first()) ? : Permission::create(['name' => 'exporte']) ; 

    }
}